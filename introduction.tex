\chapter{Introduction}
\label{ch:introduction}
Organismal senescence in humans reduces the body's ability to combat disease and maintain homeostasis as it gets older; generally leading to a gradual decline in health. In the past, lower life expectancy and higher birth rates meant the percentage of the population at risk of age related illnesses was low; the healthcare demands of this demographic were not a significant concern.
However, the average age of the world's population is increasing at an ``unprecedented'' rate \cite{WPA2002} due to the double effect of falling fertility rates and rising life expectancy which has created a trend towards 21\% of the world population being over the age of 60 by the year 2050, equalling the the population aged below 15 years old. The trend is more severe in developed nations where the over 60s are expected to make up 34\% of the population, up from 19\% in 2000, and healthcare systems must evolve to meet the demands of this new demographic profile. This has encouraged research into new medical practices that can efficiently and effectively monitor and treat age related illnesses while reducing the cost of healthcare delivery. The concept of pervasive healthcare aims to remove the constraints on where and for how long a patient can be monitored by integrating the latest sensor, wireless communication and ubiquitous computing technology with Body Sensor Networks (BSN) \cite{Yang2006}.
\nomenclature{BSN}{Body Sensor Networks}

Continuously monitoring the physiology of a patient provides two major benefits over traditional episodic measurements: the ability to detect temporally brief events (e.g. heart attacks and strokes) and the acquisition of a rich data set for long term analysis of either a patient's recovery from an operation or general well-being. However, implementing a pervasive monitoring system is not a trivial task. Such a system must satisfy the requirements of the patient as well as the healthcare professionals and financial stakeholders. Healthcare systems are very risk averse and must conform to stringent specifications to ensure that no device will ever compromise the health or safety of a patient or carer at any time. While less critical than safety requirements, usability requirements are still important and must be addressed to ensure that the patients actually want to use the monitoring devices and that the carers can gather useful information from the system. It is these base requirements that lead us towards small, low power, wireless sensor design to encourage patients to embrace the technology.

Modern sensor technology allows us to measure a wide range of physiological symptoms, but cardiovascular function is the most indicative of patient health. Cardiovascular diseases (CVDs) are responsible for 31\% of all deaths each year and in 2008, of the 17 million deaths globally due to CVDs, 15 million victims were aged over 60 \cite{WHO2011}. It is therefore crucial for a pervasive healthcare monitoring system to have the ability to sense cardiovascular function continuously.
\nomenclature{CVD}{Cardiovascular Disease}
Phonocardiography and electrocardiography are both non-invasive methods of monitoring the heart, but are restricted to monitoring the mechanical and electrical properties of the heart respectively. Photoplethysmography (PPG) is another non-invasive sensing method which measures how the optical absorption and scattering properties of blood in the capillaries varies during the systole phase of the cardiac cycle. The dynamic changes in volume of blood allow heart rate to be measured by timing the interval between pressure waves propagating through the vascular system. Measuring the transit time of a pressure wave between two sites also allows blood pressure to be inferred. The spectral properties of the blood allow the percentage of oxygen saturation in the blood to be determined (S$_{\mathrm{P}}$O$_{\mathrm{2}}$) - a technique called pulse-oximetry. These sensing modalities have made PPG a popular bed-side monitoring solution. The ability to sense oxygenation as well as blood volume dynamics is so useful that WHO have begun the ``Patient Safety Pulse Oximetry'' project to delivery pulse oximeters into operating theatres in developing nations.
\nomenclature{PPG}{Photoplethysmography}
\nomenclature{S$_{\mathrm{P}}$O$_{\mathrm{2}}$}{Oxygen saturation}

This brings up the issue of the global divide between nations with the wealth to deliver effective healthcare systems and those without. In context, CVDs are both treatable and preventable although the costs of delivering the treatment means that not everywhere or everyone has access to the same level of healthcare. Only 4\% of CVDs deaths were considered premature for high income countries, but this rises to 42\% for low income countries \cite{WHO2011} which means there is significant motivation to produce pervasive cardiac monitoring systems that are also low in cost such that wealth is not a barrier to the health benefits that these system can deliver.
\nomenclature{WHO}{World Health Organisation}

Pulse oximeters are experiencing the same trend towards miniaturisation as other electronic consumer goods as can be observed in figure~\ref{fig:pulseox_transitions}.
\begin{figure}[tbp]
	\centering
	\includegraphics[width=0.8\textwidth]{figures/pulseox_transitions}
	\caption{Transistions in size of pulse oximeters. From top to bottom: Nellcor N-180, Masimo Rad-5 and Nonin Wrist-Ox 3150.}
	\label{fig:pulseox_transitions}
\end{figure}
However, transferring pulse oximetry to a truly wearable, or even implantable, solution is non-trivial due to requirements on performance, size, longevity of operation - all while operating in environments which can be hostile to optical sensing techniques.

A truly pervasive healthcare monitoring system will provide a framework for analysing and interpreting data as well as providing a continuous sensing solution. The traditional acquire-and-transmit method for gathering and processing data is far too reliant on communications. Transmitting data wirelessly is the prime consumer of energy and needs to be minimised. Moving the data processing tasks on to the sensor node will reduce the usage of power hungry communications and also allows the node to respond directly to its ambient environment if required to do so. This research aims to address the requirements of a pervasive sensing system through the application of novel techniques at both the circuit and the system level.

\section{Requirements for Pervasive Healthcare Monitoring}
The first and foremost requirement of any healthcare delivery system is that it must improve the well-being of the patient. A monitoring system may not have any direct effects on the health of a patient, but supplying useful information allows healthcare professionals to make more informed decisions and hence prescribe more effective treatments. In some cases the monitoring system may provide direct feedback to the patient allowing them to observe how their health is affected by their lifestyle.

Equally as important is the effect a device has on the safety of a patient. It is rarely feasible to guarantee any given device will not fail to perform its intended task, but the negative impact of a failure on the well being of the user must be minimised - and must never outweigh the benefits of using the treatment in the first place. To this end there are regulations by which medical devices must conform before they can be sold and used publically. In the UK the Medicines and Healthcare products Regulatory Agency (MHRA) enforce the European Community regulations regarding the conformity of medical devices to safety guidelines.

Following safety are the more aesthetic requirements of the end user. No patient will want to use a device that is uncomfortable to wear or that requires regular attention for maintenance issues such as battery charging or cleaning. Hence, it is directly from the user requirements that the primary physical requirements of size and operational lifetime are determined.

Finally the commercial aspects need to be considered. An intentional benefit of pervasive sensing is that it reduces the labour costs involved with healthcare monitoring by allowing the sensor networks to perform the role autonomously. This approach is feasible as the cost of manufacturing these sensor devices can be minimised by taking advantage of the economies of scale associated with producing integrated-circuit based electronic solutions.

There are many technical requirements which derive from these fundamental requirements. When specifying a healthcare delivery system it is important to accurately capture these requirements to ensure that all stakeholders and regulators are satisfied with the end product. The table below summarises the requirements which pose some of the key research challenges in the development of BSN for pervasive healthcare.
\begin{table}[ht]
	\begin{minipage}{\linewidth}
		\centering
		\small
		\caption{Requirements of a pervasive healthcare monitoring system and possible solutions.}
		\label{tab:hcreqs}
		\begin{tabular}{m{4.5cm}m{7.5cm}}
			\toprule
			\multicolumn{1}{c}{\textbf{Requirement}} & \multicolumn{1}{c}{\textbf{Approaches}}\\
			\midrule
			Intuitive data visualizations &
			Data abstraction, information rich graphical user interfaces, decision support systems\\\hline
			High quality information &
			Context-based sampling strategy, conformity with approved medical systems\\\hline
			Privacy	& %
			Data encryption, data anonymity, bio-inspired immune systems, privacy preserving data mining\\\hline
			Disease-specific sensing &
			Sensing modalities and algorithms targeted to specific patient conditions\\\hline
			Variable subject lifestyle &
			Sensing modalities and algorithms adaptable to different lifestyles, physically robust sensors, on-sensor data buffering for when out of network range\\\hline
			Scalability &
			Distributed processing, on-sensor processing, autonomic system design\\\hline
			Fault tolerance	&
			Hardware and software failure recovery, feature redundancy in communications, autonomic system design\\\hline
			Communication reliability &
			On-sensor data buffering, autonomic system design\\\hline
			Ergonomics and wearability &
			Hardware miniaturization, tactile and unobtrusive devices, wireless communication\\
			\bottomrule
		\end{tabular}
		\\\footnotesize\textit{Adapted from the Wireless Sensor Network chapter written by the author and published in the Handbook of Healthcare Delivery Systems \cite{Yih2010}}
	\end{minipage}
\end{table}

From that list of requirements the one that most urgently needs addressing for wearable PPG based cardiac monitoring system is \emph{ergonomics and wearability} as this drives whether or not the patient is likely to adopt the technology. There is also the requirement for \emph{high quality information} which any instrumentation system must abide by in order to ensure that the device delivers a benefit to the patient. With these two requirements in mind the following four key challenges have been outlined for research into a solution optimised for wearable PPG sensor systems.
\newpage
\begin{enumerate}[label=\textbf{\arabic*}.]
\item \textbf{Low power opto-electronic instrumentation} \hfill \\
The fact that PPG requires active illumination of the patient's skin means that it can never feasibly be made as low power as passive cardiac sensing modalities such as ECG, but it should be possible to reduce the power to the point where the benefits of PPG outweigh the costs. To minimise the amount of optical energy emitted by the illuminator the gain of the photodetector needs to be maximised. Normally this requires using a chain of multiple electronic amplifiers or relying on a full custom integrated circuit (IC) solution.
\nomenclature{ECG}{Electrocardiography}
\nomenclature{IC}{Integrated Circuit}
Amplifier chains need too many discrete components to allow them to be implemented in a small form factor PCB and the cost of developing an IC can often be too prohibitive to consider using. By moving away from standard photodiode amplifier circuits it should be possible to find a solution that can be implemented with the minimum number of components.
\nomenclature{PCB}{Printed Circuit Board}

\item \textbf{Robust physiological signal extraction} \hfill \\
Ideally a PPG system would only measure the change in light levels due to light from the sensor's illumination module being absorbed by the wearer's arterial capillary blood, but there are countless independent light sources which the PPG will detect photons from and the optical path of the sensor contains a complex mix of different tissue and fluid components. The wearable PPG system will experience these complexities to a more severe degree than that of a PPG system used in the relatively controlled environment of a medical clinic.
Investigation is required into techniques that can lock on to both the correct source of light and the correct mode of optical absorption.

\item \textbf{High resolution data at low data rates} \hfill \\
A pervasive healthcare system requires that its constituent sensor nodes exhibit some degree of autonomy in choosing what and how much data to send to an observer without saturating the network with data that actually has little useful information content. General purpose techniques used for compressing data may not be suitable due to their requirement for memory and processing power that a low power sensor node might not be able to provide.
As such it is necessary to select only the most relevant and accurate information from the raw physiological data based on the context of the measurement situation.

\item \textbf{Miniaturised physical form factor} \hfill \\
It is not possible to accurately recreate the environment of an outwardly mobile patient in the confines of a research lab, nor is it possible to expect a patient to wear an unwieldy, hand built, electronic instrumentation system on their body and still be able to carry on with their life unhindered. Whatever solution is chosen to solve the preceding challenges it must be synthesisable in to a form factor smaller than anything that has preceded it.
\end{enumerate}

\section{Thesis Outline}
Here is an outline of the remaining chapters with a summary of each one's contribution to this thesis.
\begin{description}
\item[2. Pervasive Cardiac Monitoring] introduces the reader to any background knowledge that is required as a precursor to the research presented in later chapters. Included is a summary of the most up to date technology and research that is used in the application of photoplethysmography to pervasive healthcare monitoring.
\item[3. Integrating Photocurrent Amplifier] presents an underused alternative to the form of transimpedance amplifier conventionally used for implementing the photodetection circuitry in a PPG system. The integrating form of the transimpedance amplifier is shown to be capable of achieving very high gain while providing superior bandwidth which permits measurement of physiological signals with high temporal resolution. Rigorous modelling of the gain and noise parameter, in comparison to the conventional transimpedance amplifier, simplifies the design procedure of what is an inherently unstable circuit.
\item[4. Optical Homodyne Sensing] investigates the separation of independent light sources from the PPG sensor's own illumination circuit using frequency domain techniques. Synchronous photocurent detection is a widely used technique in PPG, but homodyne sensing is a generalised solution which can be implemented almost entirely in the discrete-time domain. This means it can work in conjunction with time division multiplexing of multiple optical wavelengths and can be embedded in firmware on a microcontroller such that no additional analogue circuitry is required.
\item[5. Multi-Mode Artefact Rejection] builds on top of homodyne sensing to use the redundant region of the signal spectrum to cancel out any independent light sources that are so high in magnitude that they would saturate the analogue front end circuitry, rendering subsequent artefact rejection useless. Having removed all linearly combined noise sources, a technique for rejection of non-linear noise sources, such as motion artefact, is investigated for its applicability to reflection mode PPG that is used predominantly in wearable solutions.
\item[6. On-Node Signal Delineation] aims to take the output signal from the artefact rejection system and select only the samples which best represent the underlying physiological waveform. Delineation is chosen as the non-stationary nature of biosignals may require high temporal resolution and very little frequency domain information. The cyclic nature of cardiac derived signals is embedded into the core function of the delineation machine which is also has programmable granularity to allow trade-offs between signal reconstruction accuracy and data rate.
\item[7. A Complete Wearable Photoplethysmography System] presents an embodiment of all methods previously described in a single printed circuit board that has been targeted for implementation in a wearable patch. The sensor provides best-in-class size and power consumption due to the possibility of implementing most of the system's functionality in a contemporary mixed-signal microcontroller. Also presented is an equivalent integrated circuit solution that uses general purpose analogue building blocks to implement not only the proposed PPG system, but a single chip that can be configured for many different sensing modalities.
\item[8. Conclusions] provides an executive summary of the main achievements of this thesis.
\end{description}

\newpage
\section{Original Contributions}
The following items highlight how the research presented in this thesis has made a positive contribution to field of wearable PPG systems.
\begin{itemize}
\item Analysis of, and design methodology for, integrating transimpedance amplifier circuits with respect to large parasitic input capacitance and micro-power operational amplifiers.
\item Homodyne light detection system with all signal processing performed in the discrete time domain.
\item DC light rejection with high dynamic range and fast response using remainder bandwidth from homodyne detection.
\item Efficient algorithm for reducing transmission data rate using delineation with complementary signal reconstruction method at receiver.
\item Implementation of the PPG photometric instrumentation circuitry using general purpose integrated circuit current conveyor cell.
\end{itemize}

\newpage
\section{Publications}
The following publications were produced from the research carried out in the process of creating this thesis.
\begin{description}
\item[1.] J.~A. Patterson, D.~C. McIlwraith, and G.-Z. Yang, ``{A Flexible, Low Noise
  Reflective PPG Sensor Platform for Ear-Worn Heart Rate Monitoring},'' in {\em
  2009 Sixth International Workshop on Wearable and Implantable Body Sensor
  Networks}, pp.~286--291, IEEE, June 2009. \cite{Patterson2009}
\item[2.] J.~A.~C. Patterson, R.~Ali, and G.-Z. Yang, ``{Wireless Sensor Network},'' in
  {\em Handbook of Healthcare Delivery Systems} (Y.~Yih, ed.), ch.~46,
  pp.~46--1--46--18, CRC Press, 2010. \cite{Yih2010}
\item[3.] J.~A.~C. Patterson and G.-Z. Yang, ``{Ratiometric Artefact Reduction in Low
  Power, Discrete-Time, Reflective Photoplethysmography},'' in {\em 2010
  International Conference on Body Sensor Networks}, pp.~174--179, IEEE, June
  2010. \cite{Patterson2010a}
\item[4.]J.~A.~C. Patterson and G.-Z. Yang, ``{Ratiometric Artifact Reduction in Low
  Power Reflective Photoplethysmography},'' {\em IEEE Transactions on
  Biomedical Circuits and Systems}, vol.~5, pp.~330--338, Aug. 2011. \cite{Patterson2011}
\item[5.] J.~A. Patterson and G.-Z. Yang, ``{Dual-Mode Additive Noise Rejection in
  Wearable Photoplethysmography},'' in {\em 2012 Ninth International Conference
  on Wearable and Implantable Body Sensor Networks}, pp.~97--102, IEEE, May
  2012. \cite{Patterson2012}
\end{description}