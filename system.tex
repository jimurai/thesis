\chapter{A Complete Wearable Photoplethysmography System}
\label{ch:system}
Having researched each component of the PPG system individually it is possible to integrate them into a single platform. The first platform in this chapter is another PCB implementation that is a physically minimised embodiment of the system that uses a state-of-the-art mixed-signal MCU to allow the footprint to be shrunk to less than 250mm\textsuperscript{2} (compared to $\sim$254mm\textsuperscript{2} for a UK 5p coin) for integration into a wearable patch enclosure. The second platform is a full-custom integrated circuit solution designed to have a general purpose architecture for sensing applications where one of the first target applications is a PPG system.

\section{Smart-Patch Compatible PCB}
One of the great benefits of reflection mode PPG is the range of locations on which it can be placed, but this creates an additional dimension when choosing how the opto-electronics are to integrate with the sensor enclosure. The ``smart-patch'' is an attractive solution because it is as location agnostic as reflection mode PPG.

\subsection{Modular Platform}
The Pervasive Sensing group at the Hamlyn Centre is also actively researching wearable ECG, glucose monitoring and other wearable sensor solutions so it was decided to target fitting the PPG sensor into a similar enclosure as had been proposed for those other sensor systems. Figure~\ref{fig:printed_patch} shows a 3D printed prototype of a silicone patch that has a cavity for containing the electronics as well as tabs for secure adhesion.
\begin{figure}[tbp]
	\centering
	\includegraphics[width=\textwidth]{figures/printed_patch}
	\caption{A prototype a smart-patch with a cavity for electronics.}
	\label{fig:printed_patch}
\end{figure}
While this is only a prototype, it was chosen by user survey as the preferred solution of several designs so is a good target platform for the PPG system to integrate into.

Given the range of low power radio solutions, batteries and sensor options it was decided to keep the system modular such that each component can be developed independently. In order to ensure that size is not compromised the PCB form factor was fixed to a maximum of 13x20mm (similar in dimensions to the Bluegiga BLE112 Bluetooth Low Energy module mentioned in the previous chapter\cite{Bluegiga}). Separate PCBs can be stacked either vertically or laid out horizontally. Unlike conventional PCB stacks there are no connectors in the design so the system configuration is fixed at build time, but there is neither the size nor the financial cost of including modular board-to-board connectors in the design. It is also feasible to adopt the patch style of Haahr et. al. \cite{Haahr2011} and make the battery and enclosure a separate sub-assembly from the electronics such that the expensive sensor components can be reused with single-use patch casings.
\begin{figure}[tbp]
	\centering
	\includegraphics[width=\textwidth]{figures/pcb_stack}
	\caption{An example PCB stack for use in a wireless smart-patch sensor.}
	\label{fig:pcb_stack}
\end{figure}
This modular approach is intended to be the optimal balance between speed of development, patient adoption and build cost.

\subsection{PPG Module}
The final version of the PCB is a two channel (IR and red) PPG system which includes a single integrating photocurrent amplifier, homodyne detection, bias current stabilisation, and signal delineation as illustrated in figure~\ref{fig:final_system}. Note that each optical channel requires its own set of bias stabilisation, homodyne detector and delineator.
\begin{figure}[tbp]
	\centering
	\includegraphics[width=\textwidth]{figures/final_system}
	\caption{Architecture of the complete PPG sensor system.}
	\label{fig:final_system}
\end{figure}
The microcontroller used is the Energy Micro\textsuperscript{\textregistered} Tiny Gecko EFM32TG210F32 which has the ARM Cortex-M3 processor core, 32kB of flash memory, and crucially includes an ADC, DAC and 3x op-amps which are required to implement the mixed signal portion of the system. Aside from passives the only other PCB components are the photocurrent amplifier's op-amp, 3x analogue switches, the photodiode and 2x 0805 footprint LEDS. The opto-electronic components are mounted on the underside of the PCB, but the photodiode can be attached via wires to implement a more flexible solution if required. The total footprint of the device is 12.5x19.5mm after removal of the debug connector which is visible in the photos of the device in figure~\ref{fig:pcb_v3}.
\begin{figure}[tbp]
	\centering
	\includegraphics[width=\textwidth]{figures/pcb_v3}
	\caption{Version 3 of the wearable PPG system PCB.}
	\label{fig:pcb_v3}
\end{figure}
Full schematics of the circuit can be found in Appendix C on page~\pageref{ch:app_c}.

\subsubsection{Performance}
The firmware was written in the C programming language and compiled using Keil $\mu$Vision v4.23 to 10,192 bytes of flash memory with only cross-source optimisation enabled, and 8,008 bytes with level 2 optimisations enabled which would allow the use of the 8kB version of the MCU which saves 10s of US cents at high volume (\textgreater 1000 units).

The inclusion of a 32kHz oscillator on the PCB allows the MCU to run in energy mode 2 when not actively processing data. When in this low power mode the core only draws 1$\mu$A of current but can transition to full operating mode in 2$\mu$s so there is little overhead associated with swapping between operating modes. While developing the system the serial port must run all of the time in asynchronous mode to support the full data throughput, but a more realistic implementation would be the sensor acting either as an I2C slave or a low speed UART (\textless 9600 baud) both of which can operate in energy mode 2. The serial port was disabled to quickly get an accurate estimate of the real power consumption when operating as an I2C slave device. Under these conditions the algorithm, timed from starting an ADC capture to being ready to transmit, executed in \textless 85$\mu$s which achieved a total average current consumption of 2.4mA at 3V giving the headline power consumption figure of 7.2mW, 5.1mW of which is consumed by the LEDs. 

\subsubsection{Output}
Figure~\ref{fig:v3_data} shows data captured by the device from the author's fingertip to prove that it is possible to capture real blood volume pulsation in such a system with so few individual components. This data was generated with an average red LED current of 1.5mA and an average IR LED current of 0.2mA. At these illumination levels the cardiac signal has a typical peak-to-peak signal of 3.5nA for the IR channel against a background noise of \textless 0.5nA (1.5nA peak signal for the red channel).
\begin{figure}[tbp]
	\centering
	\includegraphics[width=\textwidth]{figures/v3_data}
	\caption{Data output by the sensor patch PCB to validate operation. Measured at the finger tip with total LED current consumption \textless 2mA.}
	\label{fig:v3_data}
\end{figure}

\subsubsection{Battery Options}
In chapter~\ref{ch:background} a 110mAh battery was provided as an example cell that could be used in BSN applications. While the quoted capacity of batteries cannot necessarily be used to accurately estimate a system's operating life the 2.4mA consumption of this system suggests an operating life (excluding radio) of over 40 hours. However, at 15x28mm this battery has a considerably larger area (420mm\textsuperscript{2}) than the sensor itself (254mm\textsuperscript{2}). A battery equivalent in area to the sensor PCB can be sourced from manufacturers such as China BAK Battery, Inc. (\url{http://www.bak.com.cn}). One such battery is the BAK381221P 55mAh cell which measures 3.8x12x21mm and, in theory, could operate the PCB (again, excluding radio) for over 20 hours which would allow the sensor to be operated from waking till sleeping for most patients. Having to recharge everyday means that the sensor now has a usage profile like that of a smart phone. The necessity to interact with the sensor at least once per day may be considered a significant barrier to adoption by many users, especially considering how many more functions a smart phone provides for the same level of maintenance. Also, this 55mAh battery is equivalent in size to the PCB making it the dominant consumer of space. The combined influence of size and operating life time of the battery highlight it as a major barrier to the adoption of wearable sensing technology as it currently stands.

\subsection{PCB Summary}
Significant effort was required to transfer all of the advancements developed throughout this thesis to the new MCU platform so no research studies have yet been performed with the complete system. Before taking the sensor to trials it still needs to be integrated with a radio and battery into a functional patch-type enclosure. However, when it does come to trials this platform will provide more precise cardiac signal capture, for longer periods of time, and with more robustness against the environment outside of the research lab.

The bill-of-materials for this design comes to about \$8.20 (excluding passives and PCB) of which 29\% is from the MCU and 40\% is from the external 12bit dual DAC used to bias the transimpedance amplifier (see figure~\ref{fig:biased_detector}). Having one component that performs such little functionality for such a large share of the cost does not translate well to the high volume manufacturing scale that would be needed to support pervasive cardiac monitoring on a global scale. Of course there is scope for further optimisation, e.g. fixing the voltage bias and moving to the physically larger EFM32TG230 MCU which has 2 internal DACs, but for such a specific application at such a large production scale the costs justify moving towards a fully integrated circuit solution.

\section{Towards an ASIC Implementation}
The Pervasive Sensing group at the Hamlyn Centre are collaborating with Hong Kong firm ASTRI (\emph{Applied Science and Technology Research Institute}) to produce a mixed-signal system-on-chip ASIC for BSN applications, both wearable and implantable, based on the ARM Cortex-M0 MCU IP. The work presented in this chapter reflects part of the Hamlyn Centre's contribution to the project. At the time of publishing this thesis the chip had not yet been produced so only simulation results will be presented. An acknowledgement must be made to Dr. Ching-Mei Chen who joined the group late in the design phase and with her expertise in analogue IC design translated the designs from research concepts into truly synthesisable circuits.

Up to this point significant research has gone in to finding a way to move most of the PPG system functionality into the digital domain to reduce the number of discrete components in the system. This design procedure does not necessarily carry over to IC design where it is possible to implement the entire system on a single chip so the choice of where to divide the analogue and digital domains can be made based on power and sensitivity parameters as opposed to size. The simplicity of all components in the PPG system means that they could all be translated to the analogue domain, but that would constrict the ASIC to just performing PPG functions. The two functions which could conceivably be transferable to other sensing modalities are the amplification and the homodyne detection modules. The rest of the functionality can be trivially implemented in the MCU core of the ASIC.

\section{Photocurrent Amplification}
Directly translating the PCB-based integrator design into an IC-based design will definitely reduce the power consumption, but to realise the full benefits of an IC-based implementation it is worth reconsidering the circuit architecture with regards to the fundamental requirements. The first question to answer is do we still want to use the integrating amplifier? Kim et. al. have compared the capacitive feedback amplifier to the conventional resistive feedback amplifier in IC form for biomedical applications \cite{Kim2010,Kim2012} and their findings back up the findings presented in chapter~\ref{ch:integrator}; that the integration of small currents can be advantageous from a signal to noise ratio perspective as long as correlated double sampling is used to reject low frequency noise.

Back in section~\ref{ch:background:transimpedance} it was stated that the op-amp in the transimpedance amplifier performed two functions: input voltage stabilisation and current-to-voltage conversion/amplification. The combination of functionality using a general purpose amplifier leads to certain design complications. For example, the gain-bandwidth limitations caused by large parasitic input capacitance are entirely due to output of the amplifier being connected directly to the input. Additionally, the resetting of the integration capacitor is complicated by the act of shorting the input node to the amplifier output. Due to these observations it was decided to separate the tasks of voltage stabilisation and current amplification for implementing an IC transimpedance amplifier. There already exists a circuit that can perform these two functions independently: \emph{the current conveyor}.

\subsection{Current Conveyor}
First proposed by Sedra and Smith back in 1968 \cite{Smith1968}, the current conveyor is an elementary circuit building block with 3 ground referenced terminals: $X$, $Y$, $Z$. This first generation current conveyor (CCI) implemented the following rules:
\begin{enumerate}
\item The voltage at $X$ is to be the same as the voltage applied to $Y$.
\item The current into $Y$ is to be the same as the current into $X$.
\item The current into $Z$ is to be the same as the current into $X$.
\end{enumerate}
Rule 3 gives the circuit its name: current is \emph{conveyed} from node $X$ to node $Z$. Only rules 1 and 3 are required in order to implement a transimpedance amplifier. Figure~\ref{fig:cci_tia} shows a CMOS implementation of a CCI connected as a conventional transimpedance amplifier. The resistor which sets the transimpedance gain, $R_f$, is no longer connected to the input node so can now be considered independent of the input capacitance. The amplifier bandwidth is now a function of the internal bandwidth of the CCI between nodes $X$ and $Z$ as well as the combination of $R_f$ with any parasitic capacitance between node $Z$ and ground. Node $Y$ can then be used to set a static bias voltage across the photodiode without it being added to the output voltage as is the case with the voltage-mode op-amp based transimpedance amplifier.
\begin{figure}[tbp]
	\centering
	\includegraphics[width=\textwidth]{figures/cci_tia}
	\caption{A first generation current conveyor circuit and its application as a conventional transimpedance amplifier.}
	\label{fig:cci_tia}
\end{figure}

The current into node $Y$ is superfluous to the requirements of a transimpedance amplifier and hence so is rule 2 of the CCI. This ties in perfectly with the second generation current conveyor (CCII) as proposed by Sedra and Smith in 1970 \cite{Sedra1970} which is often described by the following matrix equation:
\nomenclature{CCII}{Second Generation Current Conveyor}
\begin{equation}
	\begin{bmatrix}
		v_X\\[0.3em]
		i_Y\\[0.3em]
		i_Z
	\end{bmatrix}
	= \begin{bmatrix}
		0 & 1 & 0 \\[0.3em]
		0 & 0 & 0 \\[0.3em]
		\pm1 & 0 & 0
	\end{bmatrix}
	\begin{bmatrix}
		i_X\\[0.3em]
		v_Y\\[0.3em]
		v_Z
	\end{bmatrix}
\end{equation}

Now no current flows through node $Y$ and it is possible to have both non-inverting and inverting versions of the conveyor (CCII+ and CCII- respectively). Figure~\ref{fig:ccii} shows a CMOS implementation of a CCII$\pm$ built around a voltage-mode op-amp where the current conveying is performed by using current mirrors in line with op-amp supply nodes.
\begin{figure}[tbp]
	\centering
	\includegraphics[width=\textwidth]{figures/ccii}
	\caption{A second generation current conveyor circuit implemented using an op-amp to servo the voltage on node X. Complementary output nodes are included.}
	\label{fig:ccii}
\end{figure}
The second generation current conveyor can be used as a general purpose element for the implementation of a wide range of circuit and computational functions \cite{Toumazou1992}. However, the current feedback operational amplifier is the most common usage of the current conveyor due to the very high bandwidth they can provide for a given current consumption independently of the amplifier gain and input capacitance when compared to conventional voltage feedback amplifiers. A commercially available example of a current feedback op-amp is Analog Devices' AD844, but with a quiescent current of 1mA, a full power bandwidth of 300MHz and 5pA/$\sqrt{\text{Hz}}$ input referred noise current is over-specified for a low power PPG system. This does however suggest that much lower current consumptions can be achieved for a much lower bandwidth implementation of  the current conveyor if the noise current can be kept low enough. Along with high input current noise, the improved bandwidth comes at the cost of poor input current and voltage offsets, but as the PPG system has to be designed independently of input offsets anyway these disadvantages are relatively inconsequential in the design of the photocurrent amplifier so the CCII+ is a good match to the system requirements. In fact, Kim et. al. also compared the current conveyor to \cite{Kim2010,Kim2012} the transimpedance amplifiers although their results suggested that the input referred noise was an order of magnitude worse than for the capacitive feedback transimpedance amplifier. This is liekly due to the use of the current conveyor for current amplification rather than using transimpedance gain at the output to boost the signal to noise ratio.

A further benefit of the current conveyor is that it is a current-mode circuit and hence is suitable for low-voltage operation. This allows for implementation in deep sub-micron CMOS processes as are used for manufacturing contemporary MCU devices. Using a process that is suitable for implementing low power MCUs means that the design can be relatively transferable to a range of system-on-chip solutions. The ultimate financial reward for an ASIC implementation of a pulse oximeter module would be the ability to licence the design to manufacturers that already have target markets with existing manufacturing and delivery infrastructure. Throughout this thesis the TSMC CM018 180nm process will be used. The 180nm process can be considered to be a relatively mature process and well suited to both low power digital IC implementation and for analogue cell design. Its suitability to mixed-signal MCU is highlighted by Energy Micro\textsuperscript{\textregistered} choosing to use a 180nm process to implement their low power MCUs. The very latest MSP430 devices from Texas Instruments have moved to an ultra low leakage 130nm process in order to minimise the current draw during sleep modes, but 130nm processes are still relatively expensive for prototyping predominantly analogue designs.

The specific choice of CM018 was due to its availability on multi-project wafer (MPW) services at EUROPRACTICE, MOSIS and at the Hong Kong Science and Technology Parks which gives more financial options by having flexibility in the location of fabrication.

\subsection{CCII Implementation}
\begin{figure}[tbp]
	\centering
	\includegraphics[width=\textwidth]{figures/superOTA}
	\caption{The \emph{super-OTA}: a class-AB balanced OTA built using Flipped Voltage Followers and local common-mode feedback.}
	\label{fig:superOTA}
\end{figure}
There are a number of possible ways of implementing a CCII, but to avoid going into very fine level analogue design the CCII based transimpedance amplifier is based on the implementation which uses a voltage feedback op-amp to stabilise the voltage and current-mirrors to convey the current to the output. Sticking to a more system level design might not lead to an optimum implementation of a CCII based transimpedance amplifier, but allows for quicker implementation and analysis. 

The primary requirement of whatever topology is chosen is that it has a class-AB output stage to make the cell as general purpose as possible while still being applicable to PPG. A class-AB output stage allows rail-to-rail swing in the output voltage and the ability to both sink and source current though node Z.
One possible implementation is proposed by Elwan and Soliman \cite{Elwan1997}. The stabilisation amplifier is implemented as a balanced operational transconductance amplifier (OTA) \cite{Allen2002}. The balanced OTA is desirable for its simplicity and low voltage operation.
\nomenclature{OTA}{Operational Transconductance Amplifier} 
A further benefit for our application is that the construction of a CCII from a balanced OTA is done simply by creating copies of the output stage transistors and sharing the gate connections to create current mirrors.
The main consideration with applying a balanced OTA to a transimpedance amplifier is that the current capability of the output stage is limited by the bias current of the differential pair. Setting the bias current to match the maximum expected input current of the transimpedance amplifier means wasting energy when the input current is low. To overcome this issue adaptive biasing can be added to allow the current consumption to scale with the magnitude of the input current of the cell. However, from a standby current of 56$\mu$A the maximum current available at the output node is only 300$\mu$A - a range factor of less than 6. A more usable current output would be up to 1mA which would be capable of driving the IR LED of the PPG system - even better would be up to 6mA for driving the red LED.

To improve the ratio of quiescent to maximum output current ratio the current conveyor will be built using a balanced OTA with a flipped voltage follower (FVF, \cite{Carvajal2005}) in the input stage as proposed by Lopez-Martin as a ``super'' class-AB OTA \cite{Lopez-Martin2005a}. The FVF has also been proposed for implementing a current conveyor without the super-OTA \cite{Lopez-Martin}, but that design does not have a push-pull output which is desirable to make the current conveyor as flexible a building-block as possible.
The intention of the FVF in the super-OTA is to keep the voltage at the common node of the differential pair constant while supplying as much current as is required to satisfy the individual demands of each transistor in the differential pair. The super-OTA also uses local common mode feedback to boost gain and decrease the influence of common mode input signals on circuit operation. The circuit architecture of the super-OTA is shown in figure~\ref{fig:superOTA} where the two FVFs are highlighted in blue boxes. The cross-coupled FVFs work such that with zero differential input voltage each input transistor draws a quiescent current set by the intrinsic level-translation voltage in the FVF. Typically this is designed such that the input transistors receive the same bias current as the FVFs.
\nomenclature{FVF}{Flipped Voltage Follower} 
As the differential input voltage increases one input transistor will turn off while the current through the other one will keep increasing until the upper FVF transistor is no longer operating in strong inversion and its current supply saturates.

\subsection{Simulation}
The CCII system has been designed and simulated in the Cadence IC5.1.41 environment and the results presented here are plots from the output of that tool. It is not within the scope of this thesis to present in depth circuit analysis of the proposed BSN ASIC solution - only to justify the system within the context of a PPG application. As such the details will be kept to a minimum.

The desired performance of the CCII is based on the specification in table~\ref{tab:ppg_specification} in chapter~\ref{ch:integrator} with the parameters adapted for the current-mode nature of current conveyors. The primary requirements are that the CCII has a -3dB bandwidth greater than 400kHz in current follower mode and that the input referred noise current is less than 100fA/$\sqrt{\mathrm{Hz}}$ in the 250Hz region used for homodyne sensing. The 400kHz bandwidth is an order of magnitude larger than specified for the PCB solution such that the BSN ASIC can support much narrower current pulses should an application arise that requires it.

\subsubsection{Gain}
The gain performance is tested by simulating the current gain of the circuit from the X node to the Z node. A 100nF capacitor is connected to the output to make sure the amplifier is capable of slewing into a significantly large load. Figure~\ref{fig:ccii_sim_gain} shows that the gain extends to the 400kHz requirement. The gain peak after 1MHz is due to some gain peaking in the FVF cells whose bandwidth does not necessarily line up with the resonant points in other parts of the circuit. The gain is also slightly less than unity with 3.15\% attenuation which is insignificant in comparison with the intended transimpedance gain of $\ge$ 1MV/A. Also, PPG has no quantified outputs except in the time domain so some gain inaccuracy is quite acceptable.
\begin{figure}[tbp]
	\centering
	\includegraphics[width=\textwidth]{figures/ccii_sim_gain}
	\caption{Simulated gain response of the proposed CCII based transimpedance amplifier.}
	\label{fig:ccii_sim_gain}
\end{figure}

\subsubsection{Transient}
The phase has not been included here which is generally used to infer the stability of the amplifier. Instead, figure~\ref{fig:ccii_sim_trans} shows the transient response of the CCII configured as a transimpedance amplifier with a 1$\mu$A input square-wave at 250Hz and a 1M$\Omega$ resistor connected to the Z node to implement the transimpedance gain. This plot shows two things: there is no overshoot from a current edge with a rise time of 2$\mu$s, and there is a significant offset of 47mV in the output. The lack of any overshoot proves that the phase response in current follower mode is acceptable. The presence of overshoot is not an issue for the PPG application due to the system level resilience to additive signal artefacts in the photodetector stage. In this simulation the transimpedance gain is only 0.95MV/A suggesting the presence of finite output resistance although, as stated previously, the accuracy of the gain is not crucial to the performance of the system as a whole.
\begin{figure}[tbp]
	\centering
	\includegraphics[width=\textwidth]{figures/ccii_sim_trans}
	\caption{Simulated transient response of the proposed CCII based transimpedance amplifier.}
	\label{fig:ccii_sim_trans}
\end{figure}

\subsubsection{Noise}
The circuit simulator outputs the noise voltage spectrum at a specified node so to estimate the input-referred noise current of the CCII it is necessary to configure it as a conventional transimpedance amplifier by connecting node Z to 0V via an ideal (i.e. noiseless) 1M$\Omega$ resistor and inferring the noise current. Figure~\ref{fig:ccii_sim_noise} shows the noise voltage spectrum at node Z. In the low frequency part of the spectrum flicker noise is clearly visible with a corner frequency of about 1kHz which justifies the need for homodyne sensing. The vertical axis is plotted in decibels so, for reference, the noise floor at -164dB translates to 6.3nV/$\sqrt{\mathrm{Hz}}$. If we assume a transimpedance gain of 0.95MV/A from the transient simulation this suggests an input referred noise current of 6.6fA/$\sqrt{\mathrm{Hz}}$ which is well within the requirements of the system.
There is some gain peaking at about 800kHz, but this is below 10nV/$\sqrt{\mathrm{Hz}}$. Even down at 1Hz the noise density is still below the target of 100fA/$\sqrt{\mathrm{Hz}}$ so it can be stated that the circuit noise performance is acceptable throughout the entire frequency range.
\begin{figure}[tbp]
	\centering
	\includegraphics[width=\textwidth]{figures/ccii_sim_noise}
	\caption{Simulated noise voltage spectrum at the output of the proposed CCII configured as a resistive transimpedance amplifier.}
	\label{fig:ccii_sim_noise}
\end{figure}

\subsubsection{Power}
As the ASIC is implemented with a 180nm process the voltage supply is set to 1.8V. Low supply voltage does constrain the sensing range of a transimpedance amplifier, but the as the noise is so low it is possible to sacrifice some gain to maintain dynamic range. The only significant constraint is that the forward voltage of RED LED used in version 3 of the PPG PCB has a forward voltage of 1.9V so some external circuitry will be required so that is can be driven from a higher supply voltage.

Each FVF is biased with a 200nA current source which leads to a quiescent current draw of 1.8$\mu$A for a CCII with complimentary outputs. There are only 6 current paths which are linked to the current flowing through node X and with internal scaling the total added current draw is equal to 5x the input current. For example, an input current of 1$\mu$A (the top end of the expected photocurrent range) would draw 6.8$\mu$A and consume 12.24$\mu$W of power. This can be compared to the static current consumption of the MCP6231 op-amp used in the PCB design which at 20$\mu$A would consume 36$\mu$W if operated at an equivalent 1.8V supply.

\subsection{Alternative Transfer Functions}
As with PCB based transimpedance amplifiers, it is possible to implement alternative transfer functions with the CCII+ element by changing the element which converts current to voltage, but with the added benefit that the element does not need to be used in the feedback path. For example, an integrating transimpedance amplifier can be constructed by replacing $R_f$ of figure~\ref{fig:cci_tia} with a capacitor $C_f$. Alternatively, $R_f$ can be replaced with a diode to create a logarithmic response. For the purposes of flexibility the transimpedance element can either be an internal IC component or located off-chip, selectable with an analogue multiplexer.
\begin{figure}[tbp]
	\centering
	\includegraphics[width=\textwidth]{figures/ccii_options}
	\caption{A transimpedance amplifier with a choice of linear, integrating and logarithmic transfer functions.}
	\label{fig:ccii_options}
\end{figure}

\subsection{Differential Amplification}
Chapter~\ref{ch:background} mentioned the issue of electrostatic interference in high gain transimpedance amplifiers. Differential input transimpedance amplifiers are possible in PCB implementations \cite{Graeme1995}, but were discounted for this research due to the extra PCB area and power consumption required by the two additional op-amps. Furthermore, implementing any form of DC input current stabilisation is complicated by the presence of two input current nodes. This leaves electromagnetic screening as the only suitable solution to preventing charge injection. Figure~\ref{fig:pcb_shielding} shows the use of wire mesh to shield the pulse oximeter circuit board. The conductive mesh is bonded to the ground potential of the circuit with a solder joint on the reverse of the board. The gap between the wires is intended to allow the short wavelength optical EM waves to pass, but block the longer wavelength  noise sources as well as providing a path for the wearer to electrostatically couple to the ground net instead of the input node of the photocurrent amplifier.
\begin{figure}[tbp]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/pcb_shielding}
	\caption{Wire mesh electromagnetic shielding required to protect singled ended transimpedance amplifier.}
	\label{fig:pcb_shielding}
\end{figure}

Moving to the ASIC domain means that size and power are much less of an issue so it is possible to revisit a differential input architecture. The current conveyor is again a suitable solution as it separates the bias voltage from the input current node. This allows two independent voltage sources to define the bias voltage across the photodiode without affecting the differential measurement of current through the photodiode. The circuit diagram for the differential version of the CCII based transimpedance amplifier is shown in figure~\ref{fig:ccii_itia_diff}.
\begin{figure}[tbp]
	\centering
	\includegraphics[width=0.8\textwidth]{figures/ccii_itia_diff}
	\caption{A differential transimpedance amplifier using a pair of current conveyor circuits. A capacitor is connected to the output to perform integrating transimpedance functionality.}
	\label{fig:ccii_itia_diff}
\end{figure}

\subsection{Homodyne Detector}
Translating the PCB-based mixed-signal homodyne detector to an integrated circuit design should not be a direct copy as this would make the IC architecture very specific to sensing systems that exhibit multiplicative relationships which restricts the number of sensing applications that the ASIC could be used for. For a more general purpose circuit that still supports homodyne detection it is possible to combine the multiplication stage of product detection into the current conveyor used for the transimpedance amplification stage. To match the fact that the 4 sample carrier only has the values or +1, -1 and 0 it is necessary to design the current conveyor with complimentary outputs. For a second generation current conveyor this cell is labelled as CCII$\pm$. The system then only needs to select which of the CCII$\pm$ outputs is used as the input to the integration capacitor according to the phase of the carrier. Figure~\ref{fig:ccii_config} shows the reconfigurable CCII building block used in the BSN ASIC. To implement the discrete-time mixer node \emph{CASC\_OUT} would be used as the output node with the \emph{Z\_POLARITY} switch used to select either +1 or -1, and the \emph{CASC\_OUT\_Z\_EN} switch would override the normal \emph{Z\_OUT} signal to set the output current to 0.
\begin{figure}[tbp]
	\centering
	\includegraphics[width=\textwidth]{figures/ccii_config}
	\caption{Reconfigurable current conveyor cell. Complimentary outputs enable the implementation of the coarse product detection required for homodyne sensing.}
	\label{fig:ccii_config}
\end{figure}

One possible alternative is to implement a multiplication mode current conveyor as proposed by Hwang et. al. \cite{Hwang2009} which integrates a Gilbert multiplication cell into the current conveyor allowing an external voltage to scale the gain between the input current and the output current. This would only be needed if carrier had to be more complex than the proposed 4 sample sinusoid.

\subsubsection{Filtering}
The low-pass filter of the product detector could be implemented purely in analogue circuitry on the IC, but to make the system configurable requires increasing the complexity of the design. Also, the analogue filter would either need to operate constantly at a very low current or would need to be able to be turned on and off at the sampling events associated with the discrete-time nature of TDM used for multiple optical channels. The simpler and more generic approach would be to perform an analogue to digital conversion on the output of the integration stage and implement the filter as an LWDF in either re-configurable digital logic or in software as in the previous section. This allows the system to take advantage of the low current consumption of CMOS circuitry while no changes are occurring as well as being much more configurable than a purely analogue implementation. This trade-off is dependent on the ability to convert from analogue to digital using as little energy as possible and the ability of the processor code to enter a low power sleep mode and wake up again in a short period of time. Fortunately these are both achievable features with the BSN ASIC ADC specified to work at 180$\mu$W and the processor specified to wake-up in less than 10$\mu$s.

\subsubsection{Carrier Synthesis}
Using an ITIA for photocurrent amplification means that it is only necessary to provide PWM outputs from a logic core in order to modulate the average LED drive current. PWM is a very common peripheral for MCU cores so carrier synthesis does not need specific design consideration for IC implementation. However, the LED still needs to have its peak current controlled and for this it is acceptable to reuse the OTA core of the CCII to implement the voltage to current convertor originally proposed for homodyne detection in chapter~\ref{ch:homodyne} (figure~\ref{fig:led_driver}).

\section{IC Summary}
This section has been more of a commentary of the methods that are being used to translate the PPG system from the PCB domain to the IC design domain. The content is intended to show that it is possible to build the PPG system using only an MCU core and current conveyors. Simulation results have been presented to show that the core cells used in the ASIC meet the gain, bandwidth and noise requirements while consuming less than half the power of the equivalent op-amp based PCB design.

The primary outcome from researching analogue circuit cells for implementing a BSN ASIC has been the selection of the second generation current-conveyor as a general purpose processing element that can be used as both a sensor interface as well as signal processing elements. Being a current-mode circuit element means that it is naturally suited to low-voltage, high dynamic range applications, as well as having enhanced immunity to parasitic capacitance when compared to equivalent voltage-mode cells. This chapter has focussed on using the CCII as a photodiode interface, and has set the bias conditions of the circuit to match those requirements, but it can also be used as an interface to voltage mode sensors as well, e.g. for implementing instrumentation amplifiers for ECG applications \cite{Ayachi2008,Ferri2011}, as well as for chemical sensing applications \cite{Pookaiyaudom2010} and microelectrode arrays \cite{Livi2010}.

What still remains to be addressed in the BSN ASIC project is the application of a CCII as an information processing element for the purposes of enhancing on-node data analysis. The ability to use a current conveyor for implementing a wide range of functions, e.g. filters and rectifiers, has been known for many years \cite{Wilson1990}, but more recently they have been adopted into artificial neural network designs \cite{Zatorre2006} and the addition of a multiplication function \cite{Hwang2009} opens up many interesting opportunities for the future without having to add significantly to the current library of ASIC cells.

The \emph{super-OTA} has been chosen as the core element of the CCII in order to enhance the flexibility of the circuit. On its own the super-OTA can be used as a rudimentary op-amp, but where it excels is its ability to adapt its biasing currents in order to match the requirements of the output by using flipped voltage followers. The output stage is also trivial to mirror meaning that very little extra circuitry is required to implement a CCII with complementary outputs. As such we have implemented a cell that has \textgreater 400kHz of bandwidth while being able to sink and source over 1mA of current while requiring \textless 10$\mu$A of current under quiescent conditions.

Overall, while still in an early development phase, the CCII shows promise as a very useful, low power processing element for the implementation of pervasive sensing systems while minimising the amount of circuit elements that need to be designed.