#! /usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import host_subplot

# Simulation range
fs = 0.01
fe = 1e6
N = 10000
# f = np.linspace(fs,fe,N)
# Generate Laplace range
f = np.logspace(np.log10(fs),np.log10(fe),N)
w = 2*np.pi*f
s = 1j*w

# Generate Laplace models

# BSN2012
Ts = 1e-3
wf = 0.25*(2*np.pi)
a0 = (1<<10)/3.0
a1 = (1<<7)
a2 = 1.25/(1<<12)
R_f = 330e3
C_f = 100e-12

aT = a0*a1*a2
K = ((Ts/C_f)*(aT/R_f)+1)*wf
print "Pole at", K/(2*np.pi), "Hz"
numer = s + wf
denom = s + K
H_0 = (Ts/C_f)*numer/denom
print "DC rejection =",20*np.log10(np.abs(H_0[0])),"dB"
phase_0 = np.arctan2(H_0.real,H_0.imag)

#Group delay
tau = np.zeros_like(w)
wp = 2*np.pi*f
for i in range(len(wp)):
	tmp = 0
	if (i!=0) and (i!=len(wp)-1):
		tmp = (phase_0[i+1]-phase_0[i-1])
		if tmp<-np.pi:
			tmp+=2*np.pi
		elif tmp<0:
			tmp+=np.pi
		elif tmp>np.pi:
			tmp-=np.pi
		tmp = tmp/(wp[i+1]-wp[i-1])
	tau[i] = tmp

#Tweaked
wf = 0.025*(2*np.pi)
K = ((Ts/C_f)*(aT/R_f)+1)*wf
print "Pole at", K/(2*np.pi), "Hz"
numer = s + wf
denom = s + K
H_1 = (Ts/C_f)*numer/denom


gain = plt.figure()
host = host_subplot(1,1,1)
par = host.twinx()
host.semilogx(f,20*np.log10(np.abs(H_0)),'k',linewidth=2)
plt.ylabel('Transimpedance Gain (dB)')
#plt.semilogx(f,20*np.log10(np.abs(H_1)),'r',linewidth=2)
par.semilogx(f,phase_0*180/np.pi,'r--',linewidth=1)
#par.semilogx(f,tau,'b-',linewidth=1)
#plt.semilogx(f,np.arctan2(H_1.real,H_1.imag)*180/np.pi,'r')
par.set_ylabel('Phase (degrees)')
plt.xlabel('Frequency (Hz)')
plt.legend(['Gain','Phase'])
plt.grid(True)

plt.subplots_adjust(left=0.09,right=0.92,top=0.95,bottom=0.1)
plt.show()