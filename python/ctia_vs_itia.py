#! /usr/bin/python
import numpy as np
import scipy.constants as Const
from scipy import integrate
import matplotlib.pyplot as plt


# Simulation range
fs = 100
fe = 100e6
fs = 0.1
# fe = 10e6
fe = 1e9
N = 1000000
# Generate Laplace range
f = np.logspace(np.log10(fs),np.log10(fe),N)
# f = np.linspace(fs,fe,N)
w = 2*np.pi*f
s = 1j*w


''' Gain analysis '''
H = []
f3db=[]

# CTIA
f_p = 40e3
C_sh = 100e-12
R_f = 3e6
GBW = 1e6
A_0 = 10.0**(120.0/20)
R_sh = 1e12
R_par = R_sh*R_f/(R_sh+R_f)
# Optimise bandwith
C_f_c = np.sqrt(2*C_sh/(np.pi*GBW*R_f))
# Generate Laplace models
numer = 1
b2 = (C_f_c+C_sh)/(2*np.pi*GBW)
b1 = C_f_c + (C_f_c+C_sh)/A_0 + 1/(2*np.pi*GBW*R_par)
b0 = (1.0/R_f)*(1.0+1.0/A_0) + 1.0/(A_0*R_sh)
# b0 = (1.0/R_f)*(1.0+1.0/A_0)
denom =	s*s*b2 + s*b1 + b0
H.append(numer/denom)
w0 = np.sqrt(b0/b2)
f3db.append(w0)
print("Cf = {0:.1f}pF".format(C_f_c/1e-12))
print("Required GBW = {0:.0f}kHz".format(((f_p**2)*(2*np.pi*C_sh*R_f))/1000))
print("DC Gain = {}".format(abs(H[-1][0])))
print("Estimated Resonance = {0:.1f}kHz".format(1.0/(1000*np.pi*C_f_c*R_f)))
print("Actual Resonance = {0:.1f}kHz".format(f3db[-1]/(2000*np.pi)))


# ITIA
Ts = 500e-6
Trst = 50e-6
# Optimise bandwith
C_f_i = (Ts-Trst)/R_f
# Generate Laplace models
# 1st order op-amp integrator model
numer = s*(Ts-Trst)
b2 = (C_f_i+C_sh)/(2*np.pi*GBW)
b1 = C_f_i + (C_f_i+C_sh)/A_0 + 1/(2*np.pi*GBW*R_sh)
b0 = 1.0/(A_0*R_sh)
denom =	s*s*b2 + s*b1 + b0
H.append(numer/denom)
w0 = b1/b2
f3db.append(w0)
print("Cf = {0:.1f}pF".format(C_f_i/1e-12))
print("Required GBW = {}kHz".format(int(f_p*(1+C_sh/C_f_i))/1000))
print("DC Gain = {}".format(abs(H[-1][0])))
print("Resonance = {0:.1f}kHz".format(f3db[-1]/(2000*np.pi)))

# Plot
gain = plt.figure(0,figsize=(8,6))
plt.subplot(1,1,1)
lcolors =['b','r']
for h,lc in zip(H,lcolors):
	plt.semilogx(f,20*np.log10(np.abs(h)),linewidth=2,color=lc)
# plt.semilogx(f,20*np.log10(np.abs(H_1)),'r')
plt.grid(True)
plt.ylabel('Transimpedance Gain(dB)')
plt.xlabel('Frequency (Hz)')
plt.subplots_adjust(left=0.10,right=0.97,top=0.95,bottom=0.10)
plt.title('$A_P$=3MV/A (130dB),    Op-amp $GBW$=1MHz')
plt.legend(['CTIA', 'ITIA'],loc='upper right')
plt.savefig('ctia_vs_itia.svg',format='svg')

''' Noise analysis '''
eNO = []
eN = 28e-9
iN = 0.6e-15
k = Const.k
T =273+22

#CTIA

# Generate voltage mode feedback parameters
G_par = (R_sh+R_f)/(R_sh*R_f)
G_f = 1.0/R_f
G_sh = 1.0/R_sh
Zb_c = (G_f + s*C_f_c)/(G_par + s*(C_f_c+C_sh))
eN_O = eN*(1.0/(1+GBW*s/A_0))/(1+Zb_c)
eNO_c = np.sqrt(
	((4*k*T/R_f + iN)*H[0])**2 +
	eN_O**2
	)
eNO.append(eNO_c)
print("eNO_c = {}uVrms".format(np.sqrt(integrate.simps(np.abs(eNO_c)**2,f))*1e6))

#ITIA
eN = 28e-9
iN = 0.6e-15
Zb_c = (s*C_f_c)/(G_sh + s*(C_f_c+C_sh))
eN_O = eN*(1.0/(1+GBW*s/A_0))/(1+Zb_c)
eNO_i = np.sqrt(
	((k*T*C_f_i + iN)*H[1])**2 +
	eN_O**2
	)
eNO.append(eNO_i)
print("eNO_i = {}uVrms".format(np.sqrt(integrate.simps(np.abs(eNO_i)**2,f))*1e6))

# Plot
gain = plt.figure(1,figsize=(8,6))
lcolors =['b','r']
for e,lc in zip(eNO,lcolors):
	plt.loglog(f,np.abs(e)/1e-9,linewidth=2,color=lc)
plt.grid(True)
plt.subplots_adjust(left=0.10,right=0.97,top=0.95,bottom=0.10)
plt.title('$A_P$=3MV/A (130dB),    Op-amp $GBW$=1MHz,   $C_{sh}$=100pF')
plt.ylabel('Amplifer output noise  ($nV/\sqrt{Hz}$)')
plt.xlabel('Frequency (Hz)')
plt.legend(['CTIA', 'ITIA'],loc='upper right')
plt.savefig('ctia_vs_itia_noise.svg',format='svg')

plt.show()