from scipy import fftpack
import numpy as np
import matplotlib.pyplot as plt

# Constants
f_0 = 10.0
f_1 = 100.0
T_e = 0.2
N = 10001

# Signals
t = np.linspace(0,T_e,N)
x_0 = 0.5*np.sin(f_0*2*np.pi*t) + 0.25*np.sin(2*f_0*2*np.pi*t) + 1
x_1 = np.sin(f_1*2*np.pi*t)
x_01 = x_0*x_1

# Frequency domain equivalents
f = np.linspace(0,2,N)/(2.0*T_e/N)
fx_0 = 2*np.abs(fftpack.fft(x_0))/N
fx_0[0] *=0.5
fx_1 = 2*np.abs(fftpack.fft(x_1))/N
fx_1[0] *=0.5
fx_01 = 2*np.abs(fftpack.fft(x_01))/N
fx_01[0] *=0.5
Nlim = np.nonzero(f<150)[0][-1]

# Plot signals
fig = plt.figure(figsize=(12,8))
fig.add_subplot(2,2,1)
plt.plot(t, x_0, linewidth=2)
plt.plot(t, x_1, linewidth=1,color='r')
plt.ylabel('Amplitude',fontsize=16)
plt.xlabel('Time (s)',fontsize=16)
plt.grid(True)
plt.legend(['A','B'])
fig.add_subplot(2,2,2)
plt.plot(t, x_01, linewidth=2)
plt.ylabel('AxB',fontsize=16)
plt.xlabel('Time (s)',fontsize=16)
plt.grid(True)
fig.add_subplot(2,2,3)
plt.stem(f[:Nlim], fx_0[:Nlim],linefmt='b',markerfmt='bo',basefmt='b')
plt.stem(f[:Nlim], fx_1[:Nlim],linefmt='r',markerfmt='r^')
plt.ylabel('Amplitude',fontsize=16)
plt.xlabel('Frequency (Hz)',fontsize=16)
plt.grid(True)
plt.legend(['A','B'])
fig.add_subplot(2,2,4)
plt.stem(f[:Nlim], fx_01[:Nlim])
plt.ylabel('AxB',fontsize=16)
plt.xlabel('Frequency (Hz)',fontsize=16)
plt.grid(True)

plt.suptitle(r'A=$\frac{1}{2}\sin(10\pi t) + \frac{1}{4}\sin(20\pi t) + 1$,  B=$\sin(200\pi t)$',fontsize=20)
plt.subplots_adjust(left=0.08,right=0.97,top=0.92,bottom=0.07)
# plt.savefig('../figures/prod2sum.pdf',format='pdf')
plt.show()