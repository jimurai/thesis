#! /usr/bin/python
import sys
sys.path.append('embedded_dsp/Release')
sys.path.append('embedded_dsp/test')
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import h5py
import dsp

def homodyne_detector(lwdf,data):
    # Initialise homodyne detection tools
    _lo = np.array([1,0,-1,0],dtype=np.int32)    
    # Implement product detection
    _lo_i = 0
    out = np.empty((len(data),),dtype=np.int32)
    for i,d in enumerate(data):
        # Mix signals
        _mix = d*_lo[_lo_i]
        # LWDF signal
        _fout = lwdf.push(_mix)
        out[i] = (_fout[0]+_fout[1]+1)>>1
        # Increment LO phase
        _lo_i += 1
        if _lo_i == 4: _lo_i=0
    return out

def delineator(delin,data):
    # Extract the peak data
    t_p = 0
    iz = np.ones((2,),dtype=np.int32)*i_out[0]
    v_th = np.zeros((2,3),dtype=np.int32)
    t_int = np.zeros((4,),dtype=np.int32)
    # Peak detector output lists
    k_out = []
    p_out = []
    f_out = []
    df_out = []
    for d in data:
        t_p+=1
        if t_p<2: continue
        # Save history
        _dif = int(d-iz[1]+1)>>1
        iz[1] = iz[0]
        iz[0] = d
        # Detect peak
        _res = i_delin.write(iz[1],_dif)
        # Nom tracking?
        if _res&0x02:
            v_th[0,2] = _dif
            v_th[0,1] = iz[1]
            if _res&0x01:
                v_th[0,0] = t_int[3]
                t_int[1] = 0
            else:
                v_th[0,0] = t_int[2]
                t_int[0] = 0
        elif _res&0x04:
            if _res&0x01:
                p_out.append(3)
            else:
                p_out.append(1)
            k_out.append(v_th[0,0])
            f_out.append(v_th[0,1])
            df_out.append(v_th[0,2])
        # Deriv tracking
        if _res&0x20:
            v_th[1,2] = _dif
            v_th[1,1] = iz[1]
            if _res&0x10:
                v_th[1,0] = t_int[0]
                t_int[3] = 0
            else:
                v_th[1,0] = t_int[1]
                t_int[2] = 0
        elif _res&0x40:
            if _res&0x10:
                p_out.append(2)
            else:
                p_out.append(0)
            k_out.append(v_th[1,0])
            f_out.append(v_th[1,1])
            df_out.append(v_th[1,2])
        # Run any required integrators
        t_int += 1
    # Integrate time
    _k = 0
    _ko = []
    for i,x in enumerate(k_out):
        _k += x
        _ko.append(_k)
    out = { 'i':np.array(_ko,np.int32),\
            'f':np.array(f_out,np.int32),\
            'df':np.array(df_out,np.int32),\
            'phase':np.array(p_out,np.int32)}
    return out
    
def lip(data):
    # Fetch the masked data
    ki = data['i']
    pi = data['f']
    # Create an output timebase
    k = np.arange(ki[0],ki[-1]+1)
    # Create an empty output array
    p = np.zeros(k.shape)
    # Generate spline piecewise
    for x in range(len(ki)-1):
        # Number of samples in the interval
        _k = np.arange(ki[x],ki[x+1]+1)-ki[0]
        # Linear interpolate!
        _t = 1.0*(_k-_k[0])/(_k[-1]-_k[0])     
        p[_k] = pi[x] + _t*(pi[x+1]-pi[x])
        
    return p,k
    
def cspline(p,m,k):
    t = 1.0*(k-k[0])/(k[-1]-k[0])
    _h00 = 2*(t**3) - 3*(t**2) + 1
    _h10 = (t**3) - 2*(t**2) + t
    _h01 = -2*(t**3) + 3*(t**2)
    _h11 = (t**3) - (t**2)
    out =   _h00*p[0] +\
            _h10*m[0]*(k[-1]-k[0]) +\
            _h01*p[1] +\
            _h11*m[1]*(k[-1]-k[0])
    return out
    
def asymm_cspline(p,m,k):
    if (m[0]>0) and (np.abs(m[0])>np.abs(m[1])):
        m[0]=m[0]/2.5
#    if (m[1]<0) and (np.abs(m[1])>np.abs(m[0])):
#        m[1]=m[1]/2.5
    t = 1.0*(k-k[0])/(k[-1]-k[0])
    _h00 = 2*(t**3) - 3*(t**2) + 1
    _h10 = (t**3) - 2*(t**2) + t
    _h01 = -2*(t**3) + 3*(t**2)
    _h11 = (t**3) - (t**2)
    out =   _h00*p[0] +\
            _h10*m[0]*(k[-1]-k[0]) +\
            _h01*p[1] +\
            _h11*m[1]*(k[-1]-k[0])
    return out
    
def pchip(data):
    # Fetch the masked data
    ki = data['i']
    pi = data['f']
    mi = data['df']
    # Create an output timebase
    k = np.arange(ki[0],ki[-1]+1)
    # Create an empty output array
    p = np.zeros(k.shape)
    # Generate spline piecewise
    for x in range(len(ki)-1):
        # Number of samples in the interval
        _k = np.arange(ki[x],ki[x+1]+1)-ki[0]
        # Local timebase
        _rx = [x,x+1]
        _out = cspline(pi[_rx],mi[_rx],_k)        
        p[_k] = _out
        
    return p,k
    
def asymm_pchip(data):
    # Fetch the masked data
    ki = data['i']
    pi = data['f']
    mi = data['df']
    # Create an output timebase
    k = np.arange(ki[0],ki[-1]+1)
    # Create an empty output array
    p = np.zeros(k.shape)
    # Generate spline piecewise
    for x in range(len(ki)-1):
        # Number of samples in the interval
        _k = np.arange(ki[x],ki[x+1]+1)-ki[0]
        # Local timebase
        _rx = [x,x+1]
        _out = asymm_cspline(pi[_rx],mi[_rx],_k)        
        p[_k] = _out
        
    return p,k
 
if __name__ == "__main__":
    # Homodyne tools
    fs = 1000.0
    Ts = 1.0/fs
    wp = (2/fs)*5
    ws = (2/fs)*45
    rp = 0.1
    rs = 96 
    (order,wn)= signal.cheb2ord(wp,ws,rp,rs,analog=1)
    i_filter = dsp.Filter(order,wn,rs=rs,ftype='cheby2')
    
    # Bits per extrema (samples,f[i],f'[i],phase)
    BPE = 16+32+32+2
    
    # Scaling data
    ADC_scale = 1e9*3.0/(2.0**26)
    TIA_scale = 4.38e6
    
    # Experiment lists
    data_list = [   'BSN2012/spectrum_nice',\
                    'BSN2012/spectrum_mod',\
                    'BSN2012/fast_compensation']
#    threshold_list = [0.0001,0.001,0.01,0.1,0.25,0.5,1.0,1.5,2.0,2.5,3.0,5.0]
    threshold_list = [2.0]
    di = 0                
    
    print("Thr(nA)\tDur(s)\tExtrema\tBits\tRate(bps)")
    for i,thr in enumerate(threshold_list):
        # Transfer from HDF5
        hfile = h5py.File('rpom_data.hdf5')
        data_set = hfile[data_list[di]]
        ir = data_set[:,0]
        hfile.close()
        
        # Kill the mean
        ir = ir - (1<<9)
        
        # Use product detection
        i_out = homodyne_detector(i_filter,ir)
        
        # Define the delineators
        _threshold = int(TIA_scale*thr/(2*ADC_scale)+0.5)
        i_delin = dsp.QuadDelineator(_threshold,500)
#        i_delin.delineator.contents.delta[:] = [int(24447),int(100)]
        # Extract the peak data
        peaks = delineator(i_delin,i_out)
        
        # Interpolate the data
        [i_l,k_l] = lip(peaks) 
        [i_cs,k_cs] = pchip(peaks)
        [i_acs,k_acs] = asymm_pchip(peaks) 
        
        # Mask the 1st 1 seconds of data
        tr = np.nonzero((peaks['i']>1.0/Ts))
        tk = np.nonzero((k_cs>1.0/Ts))
        
        # Print results of the study
        lrms_error = np.sqrt(np.sum((i_l[tk]-i_out[k_l[tk]])**2)/len(k_l[tk]))
        lrms_error = 2*ADC_scale*lrms_error/TIA_scale
        rms_error = np.sqrt(np.sum((i_cs[tk]-i_out[k_cs[tk]])**2)/len(k_cs[tk]))
        rms_error = 2*ADC_scale*rms_error/TIA_scale
        arms_error = np.sqrt(np.sum((i_acs[tk]-i_out[k_acs[tk]])**2)/len(k_acs[tk]))
        arms_error = 2*ADC_scale*arms_error/TIA_scale
        print("\t\t{}\t&{:.2f}&{:.2f}&{:.2f}\\\\".format(  thr,\
                                            lrms_error,\
                                            rms_error,\
                                            arms_error))

    # Plot the data
    plt.figure(figsize=(12,8))
    ax=plt.subplot(1,1,1)
    plt.plot(k_cs[tk]*Ts,2*ADC_scale*i_out[k_cs[tk]]/TIA_scale, 'k--', linewidth=2.0)
    plt.plot(k_l[tk]*Ts,2*ADC_scale*i_l[tk]/TIA_scale, 'r-', linewidth=1.0)
    plt.plot(k_cs[tk]*Ts,2*ADC_scale*i_cs[tk]/TIA_scale, 'g-', linewidth=1.0)
    plt.plot(k_acs[tk]*Ts,2*ADC_scale*i_acs[tk]/TIA_scale, 'b-', linewidth=2.0)
    plt.plot(peaks['i']*Ts,2*ADC_scale*peaks['f']/TIA_scale, 'r^',linewidth=1.0)
    plt.ylabel('A Photocurrent (nA)',fontsize=16)
    plt.xlabel('Time (s)',fontsize=16)
#    plt.legend(['Original SIgnal','CHI'])
    plt.legend(['Input','LI','CHI','Asymmetric CHI'])
    plt.grid(True)
#    ax=plt.subplot(2,1,2,sharex=ax)
#    plt.plot(k_cs[tk],2*ADC_scale*(i_cs[tk]-i_out[k_cs[tk]])/TIA_scale, 'g-', linewidth=2.0)
#    plt.grid(True)
    plt.subplots_adjust(left=0.07,right=0.97,top=0.97,bottom=0.08)
    plt.show()