#! /usr/bin/python
import numpy as np
import scipy.constants as Const
import matplotlib.pyplot as plt


# Simulation range
fs = 0.1
fe = 10e6
N = 10000
# f = np.linspace(fs,fe,N)
# Generate Laplace range
f = np.logspace(np.log10(fs),np.log10(fe),N)
w = 2*np.pi*f
s = 1j*w
eN = 28e-9
iN = 0.6e-15
k = Const.k
T =273+22

# Cf sweep
f_p = 40e3
C_sh_nom = 1e-9
C_f_nom = 100e-12
R_f = 3e6
GBW_nom = 1e6
A_0 = 10.0**(120.0/20)
R_sh = 1e12
G_sh = 1.0/R_sh
Ts = 500e-6
Trst = 50e-6

C_f = [1e-12, 10e-12, 100e-12, 1e-9, 10e-9]
C_f_lbl = ['1pF', '10pF','100pF', '1nF', '10nF']

# Generate Laplace models
H = []
eNO = []
for c in C_f:
	# 1st order op-amp integrator model
	numer = 1
	b2 = (c+C_sh_nom)/(2*np.pi*GBW_nom)
	b1 = c + (c+C_sh_nom)/A_0 + 1/(2*np.pi*GBW_nom*R_sh)
	# b1 = c + (c+C_sh_nom)/A_0
	b0 = 1.0/(A_0*R_sh)
	denom =	s*s*b2 + s*b1 + b0
	H.append(numer/denom)
	
	# Noise
	Zb_c = (s*c)/(G_sh + s*(c+C_sh_nom))
	eN_O = eN*(1.0/(1+GBW_nom*s/A_0))/(1+Zb_c)
	eNO_i = np.sqrt(
		((k*T*c + iN)*H[-1]*s*(Ts-Trst))**2 +
		eN_O**2
		)
	eNO.append(eNO_i)

plt.figure(0,figsize=(8,6))
plt.subplot(1,1,1)
for e in eNO:
	plt.loglog(f,np.abs(e)/1e-9,linewidth=2)
# plt.semilogx(f,20*np.log10(np.abs(H_1)),'r')
plt.grid(True)
plt.ylabel('Amplifer output noise  ($nV/\sqrt{Hz}$)')
plt.xlabel('Frequency (Hz)')
plt.subplots_adjust(left=0.10,right=0.97,top=0.95,bottom=0.10)
plt.title('{}={}nF, $GBW$={}kHz'.format('$C_{sh}$',
										int(C_sh_nom/1e-9),
										int(GBW_nom/1e3)))
plt.legend(C_f_lbl)
plt.savefig('itia_noise_cf.svg',format='svg')

# Csh sweep
C_sh = [10e-12, 100e-12, 1e-9, 10e-9, 100e-9]
C_sh_lbl = ['10pF','100pF', '1nF', '10nF', '100nF']

# Generate Laplace models
H = []
eNO = []
for c in C_sh:
	# 1st order op-amp integrator model
	numer = 1
	b2 = (C_f_nom+c)/(2*np.pi*GBW_nom)
	b1 = C_f_nom + (C_f_nom+c)/A_0 + 1/(2*np.pi*GBW_nom*R_sh)
	b0 = 1.0/(A_0*R_sh)
	denom =	s*s*b2 + s*b1 + b0
	H.append(numer/denom)
	
	# Noise
	Zb_c = (s*C_f_nom)/(G_sh + s*(C_f_nom+c))
	eN_O = eN*(1.0/(1+GBW_nom*s/A_0))/(1+Zb_c)
	eNO_i = np.sqrt(
		((k*T*C_f_nom + iN)*H[-1]*s*(Ts-Trst))**2 +
		eN_O**2
		)
	eNO.append(eNO_i)

plt.figure(1,figsize=(8,6))
plt.subplot(1,1,1)
for e in eNO:
	plt.loglog(f,np.abs(e)/1e-9,linewidth=2)
# plt.semilogx(f,20*np.log10(np.abs(H_1)),'r')
plt.grid(True)
plt.ylabel('Amplifer output noise  ($nV/\sqrt{Hz}$)')
plt.xlabel('Frequency (Hz)')
plt.subplots_adjust(left=0.10,right=0.97,top=0.95,bottom=0.10)
plt.title('{}={}pF, $GBW$={}kHz'.format('$C_{f}$',
										int(C_f_nom/1e-12),
										int(GBW_nom/1e3)))
plt.legend(C_sh_lbl)
plt.savefig('itia_noise_csh.svg',format='svg')

# GBW sweep
GBW = [10e3, 100e3, 1e6, 10e6]
GBW_lbl = ['10kHz', '100kHz','1MHz', '10MHz']
H = []
eNO = []
for g in GBW:
	# 1st order op-amp integrator model
	numer = 1
	b2 = (C_f_nom+C_sh_nom)/(2*np.pi*g)
	b1 = C_f_nom + (C_f_nom+C_sh_nom)/A_0 + 1/(2*np.pi*g*R_sh)
	# b1 = C_f_nom + (C_f_nom+C_sh_nom)/A_0
	b0 = 1.0/(A_0*R_sh)
	denom =	s*s*b2 + s*b1 + b0
	# denom =	s*s*b2 + s*b1
	H.append(numer/denom)
	
	# Noise
	Zb_c = (s*C_f_nom)/(G_sh + s*(C_f_nom+C_sh_nom))
	eN_O = eN*(1.0/(1+g*s/A_0))/(1+Zb_c)
	eNO_i = np.sqrt(
		((k*T*C_f_nom + iN)*H[-1]*s*(Ts-Trst))**2 +
		eN_O**2
		)
	eNO.append(eNO_i)


plt.figure(2,figsize=(8,6))
plt.subplot(1,1,1)
for e in eNO:
	plt.loglog(f,np.abs(e)/1e-9,linewidth=2)
# plt.semilogx(f,20*np.log10(np.abs(H_1)),'r')
plt.grid(True)
plt.ylabel('Amplifer output noise  ($nV/\sqrt{Hz}$)')
plt.xlabel('Frequency (Hz)')
plt.subplots_adjust(left=0.10,right=0.97,top=0.95,bottom=0.10)
plt.title('{}={}pF, {}={}nF'.format('$C_{f}$',
									int(C_f_nom/1e-12),
									'$C_{sh}$',
									int(C_sh_nom/1e-9)))
plt.legend(GBW_lbl)
plt.savefig('itia_noise_gbw.svg',format='svg')

plt.show()