from scipy import fftpack, signal
import numpy as np
from mpl_toolkits.axes_grid1 import host_subplot
import matplotlib.pyplot as plt

# Constants
fs = 5000.0
wp = (2/fs)*5
ws = (2/fs)*45
rp = 0.1
rs = 96
# (B,A)= signal.iirfilter(5,20.0*2*Ts,rs=90,btype='lowpass',analog=1,output='ba',ftype='cheby2')
# (B,A)= signal.cheby2(5,20.0*2*Ts,rs=90,btype='lowpass',analog=1,output='ba')

#Filter
(ord,wn)= signal.cheb2ord(wp,ws,rp,rs,analog=1)
print ord, wn*fs/2
(B,A)= signal.cheby2(ord,rs,wn,btype='lowpass',analog=1,output='ba')

# Response plotting range
fi = 0.1
fe = fs
N = 100000
f = np.linspace(fi,fe,N)
wi = f/(fs/2)
(w, h) = signal.freqs(B,A,wi)
phase = np.arctan2(h.real,h.imag)

#Group delay
tau = np.zeros_like(w)
wp = np.pi*w*fs
for i in range(len(wp)):
	tmp = 0
	if (i!=0) and (i!=len(wp)-1):
		tmp = (phase[i+1]-phase[i-1])
		if tmp<-np.pi:
			tmp+=2*np.pi
		elif tmp<0:
			tmp+=np.pi
		elif tmp>np.pi:
			tmp-=np.pi
		tmp = tmp/(wp[i+1]-wp[i-1])
	tau[i] = tmp

# Plot signals
fig = plt.figure(figsize=(12,6))
fig.add_subplot(1,2,1)
plt.semilogx(w*fs/2,20*np.log10(np.abs(h)),'k-',linewidth=2)
plt.xlabel('Frequency (Hz)')
plt.ylabel('Gain Response (dB)')
plt.grid(True)
host = host_subplot(1,2,2)
par = host.twinx()
host.semilogx(w*fs/2,phase*180/np.pi,'k-',linewidth=2)
par.semilogx(w[1:-1]*fs/2,1000*tau[1:-1],'r-',linewidth=2)
host.set_ylabel('Phase Response (degrees)')
par.set_ylabel('Group Delay (ms)')
plt.xlabel('Frequency (Hz)')
plt.legend(['Phase','Group Delay'],loc='upper right',bbox_to_anchor=(1, 1.11))
plt.grid(True)

plt.suptitle('Cheybshev type-II Filter Response',fontsize=20)
plt.subplots_adjust(left=0.08,right=0.94,top=0.92,bottom=0.10)
# plt.savefig('../figures/homodyne_filter_response.pdf',format='pdf')
plt.show()