#! /usr/bin/python
import sys
sys.path.append('embedded_dsp/Release')
sys.path.append('embedded_dsp/test')
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import h5py
import dsp

def homodyne_detector(lwdf,data):
    # Initialise homodyne detection tools
    _lo = np.array([1,0,-1,0],dtype=np.int32)    
    # Implement product detection
    _lo_i = 0
    out = np.empty((len(data),),dtype=np.int32)
    for i,d in enumerate(data):
        # Mix signals
        _mix = d*_lo[_lo_i]
        # LWDF signal
        _fout = lwdf.push(_mix)
        out[i] = (_fout[0]+_fout[1]+1)>>1
        # Increment LO phase
        _lo_i += 1
        if _lo_i == 4: _lo_i=0
    return out

def delineator(delin,data):
    # Extract the peak data
    t_p = 0
    iz = np.ones((2,),dtype=np.int32)*i_out[0]
    v_th = np.zeros((2,3),dtype=np.int32)
    t_int = np.zeros((4,),dtype=np.int32)
    # Peak detector output lists
    k_out = []
    p_out = []
    f_out = []
    df_out = []
    for d in data:
        t_p+=1
        if t_p<2: continue
        # Save history
        _dif = int(d-iz[1]+1)>>1
        iz[1] = iz[0]
        iz[0] = d
        # Detect peak
        _res = i_delin.write(iz[1],_dif)
        # Nom tracking?
        if _res&0x02:
            v_th[0,2] = _dif
            v_th[0,1] = iz[1]
            if _res&0x01:
                v_th[0,0] = t_int[3]
                t_int[1] = 0
            else:
                v_th[0,0] = t_int[2]
                t_int[0] = 0
        elif _res&0x04:
            if _res&0x01:
                p_out.append(3)
            else:
                p_out.append(1)
            k_out.append(v_th[0,0])
            f_out.append(v_th[0,1])
            df_out.append(v_th[0,2])
        # Deriv tracking
        if _res&0x20:
            v_th[1,2] = _dif
            v_th[1,1] = iz[1]
            if _res&0x10:
                v_th[1,0] = t_int[0]
                t_int[3] = 0
            else:
                v_th[1,0] = t_int[1]
                t_int[2] = 0
        elif _res&0x40:
            if _res&0x10:
                p_out.append(2)
            else:
                p_out.append(0)
            k_out.append(v_th[1,0])
            f_out.append(v_th[1,1])
            df_out.append(v_th[1,2])
        # Run any required integrators
        t_int += 1
    # Integrate time
    _k = 0
    _ko = []
    for i,x in enumerate(k_out):
        _k += x
        _ko.append(_k)
    out = { 'i':np.array(_ko,np.int32),\
            'f':np.array(f_out,np.int32),\
            'df':np.array(df_out,np.int32),\
            'phase':np.array(p_out,np.int32)}
    return out
        
def dlog(data):
    out = np.zeros((len(data),),dtype=np.int32)
    _h = np.zeros((3,),dtype=np.int32)
    for i,d in enumerate(data):
        if i<2: continue
        _h[1:] = _h[:-1]
        _h[0] = d
        _dif = _h[0]-_h[2]
        _dlog = _h[1]
        if _dlog==0: _dlog=1<<8
        out[i-1] = ((_dif<<15)+((_dlog+(1<<6))>>7))/((_dlog+(1<<7))>>8)
    return out
 
if __name__ == "__main__":
    # Homodyne tools
    fs = 1000.0
    Ts = 1.0/fs
    wp = (2/fs)*5
    ws = (2/fs)*45
    rp = 0.1
    rs = 96 
    (order,wn)= signal.cheb2ord(wp,ws,rp,rs,analog=1)
    i_filter = dsp.Filter(order,wn,rs=rs,ftype='cheby2')
    r_filter = dsp.Filter(order,wn,rs=rs,ftype='cheby2')
    
    # Bits per extrema (samples,f[i],f'[i],phase)
    BPE = 16+32+32+2
    
    # Scaling data
    ADC_scale = 1e9*3.0/(2.0**26)
    TIA_scale = 4.38e6
    
    # Experiment lists
    data_list = [   'BSN2012/spectrum_nice',\
                    'BSN2012/spectrum_mod',\
                    'BSN2012/fast_compensation']
#    threshold_list = [0.0001,0.001,0.01,0.1,0.25,0.5,1.0,1.5,2.0,2.5,3.0,5.0]
    threshold_list = [2.0]
    di = 0                
    
    print("Thr(nA)\tDur(s)\tExtrema\tBits\tRate(bps)")
    for i,thr in enumerate(threshold_list):
        # Transfer from HDF5
        hfile = h5py.File('rpom_data.hdf5')
        data_set = hfile[data_list[di]]
        ir = data_set[:,0]
        red = data_set[:,1]
        hfile.close()
        
        # Kill the mean
        ir = ir - (1<<9)
        red = red - (1<<9)
        
        # Use product detection
        i_out = homodyne_detector(i_filter,ir)
        r_out = homodyne_detector(r_filter,red)
        di_out = np.zeros(i_out.shape)
        dr_out = np.zeros(r_out.shape)
        di_out[1:-1] = (i_out[2:]-i_out[:-2])>>1
        dr_out[1:-1] = (r_out[2:]-r_out[:-2])>>1
        
        # Define the delineators
        _threshold = int(TIA_scale*thr/(2*ADC_scale)+0.5)
        i_delin = dsp.QuadDelineator(_threshold,100)
        r_delin = dsp.QuadDelineator(_threshold,100)
#        i_delin.delineator.contents.delta[:] = [int(24447),int(100)]
        # Extract the peak data
        ipeaks = delineator(i_delin,i_out)
        rpeaks = delineator(r_delin,r_out)
        
        # Log conversion
        _scale =  2.*(2.0**15)*(2.0**8)
        i_dld = dlog(i_out)
        r_dld = dlog(r_out)
        i_dlf = _scale*di_out/i_out
        r_dlf = _scale*dr_out/r_out
        
        # Integrate
        i_lg = []
        r_lg = []
        t_lg = []
        _il = 0
        _rl = 0
        for n in xrange(len(ipeaks['i'])-1):
            if (ipeaks['phase'][n+1] == 0)or(ipeaks['phase'][n+1] == 1) :
                # Integrate over entire phase
                for _n in np.arange(ipeaks['i'][n],ipeaks['i'][n+1]+1):
                    _il += i_dld[_n]
                    _rl += r_dld[_n]
            if ipeaks['phase'][n+1] == 0:
                t_lg.append(ipeaks['i'][n+1])
                i_lg.append(_il)
                r_lg.append(_rl)
                _il = 0
                _rl = 0
        t_lg = np.array(t_lg)
        i_lg = np.array(i_lg)
        r_lg = np.array(r_lg)
            
            
    # Ratios at ipeaks
    t_0 = ipeaks['i'][np.nonzero(ipeaks['phase']==0)[0]]
    RoR_0 = 1.*i_dld[t_0]/r_dld[t_0]
    t_1 = ipeaks['i'][np.nonzero(ipeaks['phase']==1)[0]]
    RoR_1 = 1.*i_dld[t_1]/r_dld[t_1]
    t_2 = ipeaks['i'][np.nonzero(ipeaks['phase']==2)[0]]
    RoR_2 = 1.*i_dld[t_2]/r_dld[t_2]
    t_3 = ipeaks['i'][np.nonzero(ipeaks['phase']==3)[0]]
    RoR_3 = 1.*i_dld[t_3]/r_dld[t_3]
    
    # Ratios!
#    kr = np.nonzero(ipeaks['phase']==0)[0]
#    print kr
    RoR_4 = 1.*i_lg/r_lg
    t_4 = t_lg
#    
    h660 = 2.5
    h905 = 0.2
    o660 = 0.15
    o905 = 0.25
    
    h660 = 3226.56
    h905 = 769.8
    o660 = 319.6
    o905 = 1209.2
    
    h660 = 3.2
    h905 = 0.8
    o660 = 0.3
    o905 = 1.2

    sp_0 = 100*(h660 - h905*RoR_0)/(h660-o660+(o905-h905)*RoR_0)
    sp_1 = 100*(h660 - h905*RoR_1)/(h660-o660+(o905-h905)*RoR_1)
    sp_2 = 100*(h660 - h905*RoR_2)/(h660-o660+(o905-h905)*RoR_2)
    sp_3 = 100*(h660 - h905*RoR_3)/(h660-o660+(o905-h905)*RoR_3)
    sp_4 = 100*(h660 - h905*RoR_4)/(h660-o660+(o905-h905)*RoR_4)

    # Plot the data
    plt.figure(figsize=(12,8))
    ax=plt.subplot(1,1,1)
    plt.plot(t_0*Ts,sp_0, 'b',linewidth=2.0)
    plt.plot(t_1*Ts,sp_1, 'g--',linewidth=2.0)
    plt.plot(t_2*Ts,sp_2, 'r',linewidth=2.0)
    plt.plot(t_3*Ts,sp_3, 'm--',linewidth=2.0)
    # plt.plot(t_4*Ts,sp_4, 'k',linewidth=2.0)
    plt.legend(['$\phi_0$','$\phi_1$','$\phi_2$','$\phi_3$','$\phi_{[0,1]}$'])
    plt.grid(True)
    plt.ylabel('$S_PO_2$ (%)',fontsize=16)
    plt.xlabel('Time (s)',fontsize=16)
    # plt.subplot(2,1,2)
    # plt.plot(i_dld,'k', linewidth=1.0)
    # plt.plot(ipeaks['i'],i_dld[ipeaks['i']],'k', linewidth=1.0)
    # plt.plot(i_dlf,'r', linewidth=1.0)
    # plt.plot(ipeaks['i'],i_dlf[ipeaks['i']],'r', linewidth=1.0)
    
    plt.subplots_adjust(left=0.08,right=0.97,top=0.97,bottom=0.08)
    plt.show()