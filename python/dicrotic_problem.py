#! /usr/bin/python
import sys
sys.path.append('embedded_dsp/Release')
sys.path.append('embedded_dsp/test')
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import dsp

# Transfer from HDF5
import h5py
hfile = h5py.File('rpom_data.hdf5')
data_set = hfile['BSN2012/spectrum_nice']
ir = data_set[:,0]
red = data_set[:,1]
hfile.close()

# Kill the mean
ir = ir - (1<<9)
red = red - (1<<9)

# Filter constants
fs = 1000.0
wp = (2/fs)*5   # This has been lowered for prettiness of signals
ws = (2/fs)*45
rp = 0.1
rs = 96 

# DSP constants
PLOTDEPTH = len(ir)
Ts = 1.0/fs
N = 4
t = np.arange(PLOTDEPTH)*Ts

# Initialise homodyne detection tools
local_osc = np.array([1,0,-1,0],dtype=np.int32)
(order,wn)= signal.cheb2ord(wp,ws,rp,rs,analog=1)
i_filter = dsp.Filter(order,wn,rs=rs,ftype='cheby2')
r_filter = dsp.Filter(order,wn,rs=rs,ftype='cheby2')

# Use product detection
lo_index = 0
i_out = []
r_out = []
for i,r in zip(ir,red):
    # Mix signals
    _mix = i*local_osc[lo_index]
    # LWDF signal
    _fout = i_filter.push(_mix)
    _value = (_fout[0]+_fout[1]+1)>>1
    i_out.append(_value)
    # Mix signals
    _mix = r*local_osc[lo_index]
    # LWDF signal
    _fout = r_filter.push(_mix)
    _value = (_fout[0]+_fout[1]+1)>>1
    r_out.append(_value)
    # Increment LO phase
    lo_index += 1
    if lo_index == N: lo_index=0
i_out = np.array(i_out,dtype=np.int32)
r_out = np.array(r_out,dtype=np.int32)

# Select a pretty subset
#tr = np.nonzero((t>=7.02)*(t<=7.85))
#i_out = i_out[tr]
#r_out = r_out[tr]
#t = t[tr] -t[tr[0][0]]

# Define the delineators
i_delin = dsp.QuadDelineator(1,1)
# Extract the peak data
t_p = 0
iz = np.ones((2,),dtype=np.int32)*i_out[0]
v_th = np.zeros((2,3),dtype=np.int)
t_int = np.zeros((4,),dtype=np.int)
# Peak detector output arrays/lists
t_out = []
p_out = []
v_out = []
dv_out = []
for i,r in zip(i_out,r_out):
    t_p+=1
    if t_p<2: continue
    # Run any required integrators
    t_int += 1
    # Save history
    _dif = int(i-iz[1])
    iz[1] = iz[0]
    iz[0] = i
    # Detect peak
    res = i_delin.write(iz[1],_dif)
    # Nom tracking?
    if res&0x02:
        v_th[0,2] = _dif
        if res&0x01:
            v_th[0,0] = t_int[3]
            v_th[0,1] = i
            t_int[1] = 0
        else:
            v_th[0,0] = t_int[2]
            v_th[0,1] = i
            t_int[0] = 0
    if res&0x04:
        if res&0x01:
            p_out.append(3)
        else:
            p_out.append(1)
        t_out.append(v_th[0,0])
        v_out.append(v_th[0,1])
        dv_out.append(v_th[0,2])
    # Deriv tracking
    if res&0x20:
        v_th[1,2] = _dif
        if res&0x10:
            v_th[1,0] = t_int[0]
            v_th[1,1] = i
            t_int[3] = 0
        else:
            v_th[1,0] = t_int[1]
            v_th[1,1] = i
            t_int[2] = 0
    if res&0x40:
        if res&0x10:
            p_out.append(2)
        else:
            p_out.append(0)
        t_out.append(v_th[1,0])
        v_out.append(v_th[1,1])
        dv_out.append(v_th[1,2])
# Integrate time
_ts = 0
for i,x in enumerate(t_out):
    _ts += x
    t_out[i] = _ts
t_out = np.array(t_out)
dv_out = np.array(dv_out)//(2*Ts)
# Scale data
ADC_scale = 1e9*3.0/(2.0**26)
TIA_scale = 4.38e6
i_out = 2*ADC_scale*np.array(i_out)/TIA_scale
v_out = 2*ADC_scale*np.array(v_out)/TIA_scale
dv_out = 2*ADC_scale*np.array(dv_out)/TIA_scale
r_out = 2*ADC_scale*np.array(r_out)/TIA_scale
di_out = (i_out[2:]-i_out[:-2])/(2*Ts)

# Select regions
_t = Ts*t_out
r0 = np.nonzero((t>_t[1])*(t<=_t[3]))
r1 = np.nonzero((t>_t[3])*(t<=_t[5]))
r2 = np.nonzero((t>_t[5])*(t<=_t[7]))
r3 = np.nonzero((t>_t[7])*(t<=_t[9]))
ra = np.nonzero((t>_t[1])*(t<=_t[3]))
rb = np.nonzero((t>_t[3])*(t<=_t[9]))

# Plot the data
plt.figure(figsize=(12,8))
ax=plt.subplot(2,1,1)
plt.plot(t,i_out, 'k-', linewidth=4.0)
plt.fill_between(t[r0], np.min(i_out[r0]), i_out[r0], facecolor='b', interpolate=True,alpha=0.3)
plt.fill_between(t[r1], np.min(i_out[r1]), i_out[r1], facecolor='r', interpolate=True,alpha=0.3)
plt.fill_between(t[r2], np.min(i_out[r1]), i_out[r2], facecolor='b', interpolate=True,alpha=0.3)
plt.fill_between(t[r3], np.min(i_out[r1]), i_out[r3], facecolor='r', interpolate=True,alpha=0.3)
plt.text(0.1,274,'$\phi_{\mathrm{fall}}$',fontsize=40,ha='left')
plt.text(0.3,274,'$\phi_{\mathrm{rise}}$',fontsize=40,ha='left')
plt.text(0.4,274,'$\phi_{\mathrm{fall}}$',fontsize=40,ha='left')
plt.text(0.6,274,'$\phi_{\mathrm{rise}}$',fontsize=40,ha='left')
plt.ylabel('A Photocurrent (nA)',fontsize=16)
plt.grid(True)
plt.subplot(2,1,2,sharex=ax)
plt.plot(t,i_out, 'k-', linewidth=4.0)
plt.fill_between(t[ra], np.min(i_out[ra]), i_out[ra], facecolor='b', interpolate=True,alpha=0.3)
plt.fill_between(t[rb], np.min(i_out[ra]), i_out[rb], facecolor='r', interpolate=True,alpha=0.3)
plt.text(0.1,274,'$\phi_{\mathrm{fall}}$',fontsize=40,ha='left')
plt.text(0.45,274,'$\phi_{\mathrm{rise}}$',fontsize=40,ha='left')
plt.ylabel('B Photocurrent (nA)',fontsize=16)
plt.xlabel('Time (s)',fontsize=16)
plt.grid(True)

plt.suptitle('A = no thresholding, B = thresholding',fontsize=20)
plt.subplots_adjust(left=0.07,right=0.92,top=0.94,bottom=0.07,hspace=0.1)
plt.show()