"""
Filename: bb_dsp.py
Demonstrates the use of ctypes with the BlackBox DSP code:

"""
import numpy as np
from scipy import signal
import ctypes, sys
# Load the library as _libdsp.
# Why the underscore (_) in front of _libdsp below?
# To mimimise namespace pollution -- see PEP 8 (www.python.org).
_libdsp = None
for directory in sys.path:
    try:
        _libdsp = np.ctypeslib.load_library('libbb_dsp', directory)
        break
    except:
        pass
if _libdsp == None: raise EnvironmentError('Cannot find the DLL.')

# void lwdf_gamma2alpha(float gamma, LWDF_ALPHA *alpha, uint8_t *type)
_libdsp.lwdf_gamma2alpha.argtypes = [	ctypes.c_float,\
										np.ctypeslib.ndpointer(dtype = np.uint16),\
										np.ctypeslib.ndpointer(dtype = np.uint8)]
_libdsp.lwdf_gamma2alpha.restype =		None
# void lwdf_initFilter(LWDF_FILTER* filter, uint8_t order, LWDF_ALPHA *alpha, uint8_t *type);
_libdsp.lwdf_initFilter.argtypes = [ctypes.c_void_p,\
									ctypes.c_uint8,\
									np.ctypeslib.ndpointer(dtype = np.uint16),\
									np.ctypeslib.ndpointer(dtype = np.uint8)]# Declare arg type, same below.
_libdsp.lwdf_initFilter.restype =	None
# LWDF_FILTER* lwdf_newFilter(void)
_libdsp.lwdf_newFilter.argtypes = []
_libdsp.lwdf_newFilter.restype = ctypes.c_void_p

# void lwdf_write(LWDF_FILTER* filter, LWDF_TYPE input)
_libdsp.lwdf_write.argtypes = [	ctypes.c_void_p,\
								ctypes.c_int16]
_libdsp.lwdf_write.restype =	ctypes.c_void_p

# LWDF_TYPE* lwdf_read(LWDF_FILTER* filter)
_libdsp.lwdf_read.argtypes = [ctypes.c_void_p]
_libdsp.lwdf_read.restype = ctypes.POINTER(ctypes.c_int32)

# LWDF_TYPE lwdf_read_lpf(LWDF_FILTER* filter)
_libdsp.lwdf_read_lpf.argtypes = [ctypes.c_void_p]
_libdsp.lwdf_read_lpf.restype = ctypes.c_int32

# LWDF_TYPE lwdf_read_hpf(LWDF_FILTER* filter)
_libdsp.lwdf_read_hpf.argtypes = [ctypes.c_void_p]
_libdsp.lwdf_read_hpf.restype = ctypes.c_int32

class TP(ctypes.Structure):
	_fields_ = [("type", ctypes.c_char),
				("value", ctypes.c_int32),
				("time", ctypes.c_uint32)]
				
# TP_DETECTOR* tpd_initDetector(TP_VALUE delta);
_libdsp.tpd_initDetector.argtypes = [	ctypes.c_int32]
_libdsp.tpd_initDetector.restype =		ctypes.c_void_p

# void tpd_write(TP_DETECTOR* detector, TP_VALUE value);
_libdsp.tpd_write.argtypes = [	ctypes.c_void_p,\
								ctypes.c_int32]
_libdsp.tpd_write.restype =		None

# void tpd_setDelta(TP_DETECTOR* detector, TP_VALUE delta);
_libdsp.tpd_setDelta.argtypes = [	ctypes.c_void_p,\
									ctypes.c_int32]
_libdsp.tpd_setDelta.restype =		None

# uint8_t tpd_push(TP_DETECTOR* detector, TP* tp); // This should probably be static
_libdsp.tpd_push.argtypes = [	ctypes.c_void_p,\
								ctypes.POINTER(TP)]
_libdsp.tpd_push.restype =		ctypes.c_uint8

# uint8_t tpd_pop(TP_DETECTOR* detector, TP* tp);
_libdsp.tpd_pop.argtypes = [	ctypes.c_void_p,\
								ctypes.POINTER(TP)]
_libdsp.tpd_pop.restype =		ctypes.c_uint8

# uint8_t tpd_depth(TP_DETECTOR* detector);
_libdsp.tpd_depth.argtypes = [ctypes.c_void_p]
_libdsp.tpd_depth.restype = ctypes.c_uint8

class RawFilter():
	def __init__(self, wn, roots):
		self.wn = wn
		self.roots = roots
		self.order = len(roots)
		gammas = []
		psi = np.tan(np.pi*self.wn/2.0)
		# Separate the real pole from the complex poles
		real_index = 0;
		for i in range(self.order):
			if abs(self.roots[i].imag) <= 1e-16:
				real_index = i
				break
		complex_roots = np.concatenate((self.roots[0:real_index],self.roots[real_index+1:]))
		# Put in the real pole's gamma value
		h_B = -1.0*self.roots[real_index].real
		gammas.append((1.0 - h_B) / (1.0 + h_B))
		# Calculate coefficients of the individual Hurwitz polynomials
		for i in (range((self.order-1)/2)):
			h_A = -2.0*(complex_roots[2*i].real)
			h_B = abs(complex_roots[2*i])**2
			gammas.append((h_A - h_B - 1.0)/(h_A + h_B + 1.0))
			gammas.append((1.0 - h_B) / (1.0 + h_B))
		# Construct filter
		gammas_out = np.asarray(gammas,dtype=np.float32)
		alphas = []
		types = []
		for x in gammas:
			print '{0:.10f}'.format(x)
			alpha = np.array((1,),dtype = np.uint16)
			type = np.array((1,),dtype = np.uint8)
			_libdsp.lwdf_gamma2alpha(x,alpha,type)
			alphas.append(alpha)
			types.append(type)
		alphas_out = np.asarray(alphas,dtype=np.uint16)
		types_out = np.asarray(types,dtype=np.uint8)
		print alphas_out.transpose()
		print types_out.transpose()
		self.filter = _libdsp.lwdf_newFilter()
		_libdsp.lwdf_initFilter(self.filter,self.order,alphas_out,types_out)
			
	def push(self, value):
		_libdsp.lwdf_write(self.filter,value)
		filter_output = ctypes.c_int32*2
		filter_output = _libdsp.lwdf_read(self.filter)
		return filter_output

class Filter():
	def __init__(self, order=3, wn=0.01, rp=1, rs=60, ftype='butter'):
		if order%2 == 0:
			print 'No even order filters allowed.'
			return
		# Filter settings
		self.order = order
		self.wn = wn
		self.rp = rp
		self.rs = rs
		self.ftype = ftype
		gammas = []
		psi = np.tan(np.pi*self.wn/2.0)
		psi2 = psi*psi
		if self.ftype == 'butter':
			(z,p,k) = signal.iirfilter(self.order, psi, btype='lowpass', analog=1, ftype='butter', output='zpk')
			filter_roots = np.sort(p)
		elif self.ftype == 'bessel':
			print 'Please avoid using Bessel filters as they don\'t translate well to LWDFs.'
			(z,p,k) = signal.iirfilter(self.order, psi, btype='lowpass', analog=1, ftype='bessel', output='zpk')
			filter_roots = np.sort(p)
		elif self.ftype == 'cheby1':
			(z,p,k) = signal.iirfilter(self.order, psi, rp=1, rs=self.rs, btype='lowpass', analog=1, ftype='cheby1', output='zpk')
			filter_roots = np.sort(p)
		elif self.ftype == 'cheby2':
			(z,p,k) = signal.iirfilter(self.order, psi, rs=self.rs, btype='lowpass', analog=1, ftype='cheby2', output='zpk')
			filter_roots = np.sort(p)
		else:
			print 'Invalid filter type.'
			return
		# Separate the real pole from the complex poles
		real_index = 0;
		for i in range(self.order):
			if abs(filter_roots[i].imag) <= 1e-16:
				real_index = i
				break
		complex_roots = np.concatenate((filter_roots[0:real_index],filter_roots[real_index+1:]))
		# Put in the real pole's gamma value
		h_B = -1.0*filter_roots[real_index].real
		gammas.append((1.0 - h_B) / (1.0 + h_B))
		# Calculate coefficients of the individual Hurwitz polynomials
		for i in (range((order-1)/2)):
			h_A = -2.0*(complex_roots[2*i].real)
			h_B = abs(complex_roots[2*i])**2
			gammas.append((h_A - h_B - 1.0)/(h_A + h_B + 1.0))
			gammas.append((1.0 - h_B) / (1.0 + h_B))
		# Construct filter
		gammas_out = np.asarray(gammas,dtype=np.float32)
		alphas = []
		types = []
		for x in gammas:
			print '{0:.10f}'.format(x)
			alpha = np.array((1,),dtype = np.uint16)
			type = np.array((1,),dtype = np.uint8)
			_libdsp.lwdf_gamma2alpha(x,alpha,type)
			alphas.append(alpha)
			types.append(type)
		alphas_out = np.asarray(alphas,dtype=np.uint16)
		types_out = np.asarray(types,dtype=np.uint8)
		print alphas_out.transpose()
		print types_out.transpose()
		self.filter = _libdsp.lwdf_newFilter()
		_libdsp.lwdf_initFilter(self.filter,self.order,alphas_out,types_out)
			
	def push(self, value):
		_libdsp.lwdf_write(self.filter,value)
		filter_output = ctypes.c_int32*2
		filter_output = _libdsp.lwdf_read(self.filter)
		return filter_output
			
	def read(self):
		filter_output = ctypes.c_int32*2
		filter_output = _libdsp.lwdf_read(self.filter)
		return filter_output

class TPDetector():
	def __init__(self, delta=50):
		self.detector = _libdsp.tpd_initDetector(delta)
		
	def write(self,value):
		_libdsp.tpd_write(self.detector,value)
		
	def setDelta(self,delta):
		_libdsp.tpd_setDelta(self.detector,delta)
		
	def push(self,turning_point):
		_libdsp.tpd_push(self.detector,turning_point)
		
	def pop(self):
		turning_point = TP()
		depth = _libdsp.tpd_pop(self.detector,turning_point)
		if (depth==1):
			return [ord(turning_point.type),turning_point.value,turning_point.time]
		else:
			return [-1,0,0]
		
	def depth(self):
		return _libdsp.tpd_depth(self.detector)