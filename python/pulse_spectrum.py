from scipy import integrate
import math
import numpy as np
import matplotlib.pyplot as plt

# some constants
Ts = 1e-6
Fs = 0.001
Fe = (10.0/Ts)*2
Nf = 20000
f = np.linspace(0,Fe, Nf+1)
x = Ts*np.sinc(Ts*f)
P = x*x
N = []
E = []
for n in xrange(10):
	fsub = f < (n+1)/Ts
	N.append(n+1)
	E.append(2*integrate.simps(P[fsub],f[fsub]))
E = np.array(E)

fig = plt.figure(figsize=(8,6))
fig.add_subplot(111)
plt.plot(N, 100*E/Ts, linewidth=2)
plt.ylabel('Energy Content (%)')
plt.xlabel('Number of Included Harmonics')
plt.grid(True)
plt.subplots_adjust(left=0.10,right=0.97,top=0.95,bottom=0.10)
plt.savefig('../figures/pulse_spectrum.pdf',format='pdf')
plt.show()