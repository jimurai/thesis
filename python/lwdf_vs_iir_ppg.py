#! /usr/bin/python
import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
sys.path.append('./dsp')
import bb_dsp

# Transfer from HDF5
import h5py
hfile = h5py.File('rpom_data.hdf5')
data_set = hfile['BSN2012/spectrum_no_mod']
ir = data_set[:,0]
hfile.close()

# Filter constants
fs = 1000.0
wp = (2/fs)*5
ws = (2/fs)*45
rp = 0.1
rs = 96

# DSP constants
PLOTDEPTH = len(ir)
Ts = 1.0/1e3
N = 4
trange = np.arange(PLOTDEPTH)*Ts

# Initialise homodyne detection tools
local_osc = np.array([1,0,-1,0],dtype=np.int32)
(order,wn)= signal.cheb2ord(wp,ws,rp,rs,analog=1)
i_filter = bb_dsp.Filter(order,wn,rs=rs,ftype='cheby2')

# LWDF
i_lwdf = []
i_lwdf_16b = []
for i in ir:
	_fout = i_filter.push(i)
	_value = (_fout[0]+_fout[1]+1)>>1
	i_lwdf.append((_value))
	_value = (_fout[0]+_fout[1]+(1<<16))>>17
	i_lwdf_16b.append((_value))
i_lwdf = np.array(i_lwdf)
i_lwdf_16b = np.array(i_lwdf_16b)

# IIR
(B,A)= signal.cheby2(order,rs,wn,btype='lowpass',output='ba')
zf = signal.lfiltic(B,A,np.zeros(len(A)-1), np.zeros(len(B)-1))
i_iir, zf = signal.lfilter(B,A,ir,zi=zf)


# Scale data
#ADC_scale = 1e9*3.0/(2.0**10)
#TIA_scale = 4.38e6
#ir = ADC_scale*np.array(ir)/TIA_scale
#i_lwdf = ADC_scale*np.array(i_lwdf)/TIA_scale
#i_iir = ADC_scale*np.array(i_iir)/TIA_scale

# Plot the data
plt.figure(figsize=(12,8))
ax1 = plt.subplot(3,1,1)
plt.plot(trange,ir, 'k-', linewidth=2.0)
plt.ylabel('Input Signal',fontsize=14)
plt.grid()
ax2=plt.subplot(3,1,2, sharex=ax1)
plt.plot(trange,i_lwdf/2.0**16, 'k-', linewidth=2.0)
plt.plot(trange,i_iir, 'r--', linewidth=2.0)
plt.ylabel('Filtered Signal',fontsize=14)
plt.grid()
ax3=plt.subplot(3,1,3, sharex=ax1)
error = 100.0*(i_lwdf/2.0**16-i_iir)/i_iir
plt.plot(trange,error, 'r-', linewidth=2.0)
plt.ylabel('Error (%)',fontsize=14)
plt.xlabel('Time (s)',fontsize=14)
plt.grid()
#plt.suptitle('LWDF vs. IIR Filter Response',fontsize=20)
plt.subplots_adjust(left=0.09,right=0.97,top=0.97,bottom=0.07,hspace=0.14)

plt.figure(figsize=(10,6))
ax4=plt.subplot(2,1,1)
plt.plot(trange,i_iir, 'r-', linewidth=2.0)
plt.plot(trange,i_lwdf_16b, 'k-', linewidth=2.0)
plt.ylabel('Filtered Signal',fontsize=14)
plt.grid()
ax5=plt.subplot(2,1,2, sharex=ax4)
plt.plot(trange,i_lwdf_16b-i_iir, 'r-', linewidth=2.0)
plt.ylabel('Error',fontsize=16)
plt.xlabel('Time (s)',fontsize=14)
plt.grid()
plt.subplots_adjust(left=0.09,right=0.97,top=0.97,bottom=0.08,hspace=0.14)

plt.show()