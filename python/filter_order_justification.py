#! /usr/bin/python
import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
sys.path.append('./dsp')
import bb_dsp

# Transfer from HDF5
import h5py
hfile = h5py.File('rpom_data.hdf5')
data_set = hfile['BSN2012/spectrum_nice']
ir = data_set[:,0]
hfile.close()

# Use product detection
lo_index = 0
local_osc = np.array([1,0,-1,0],dtype=np.int32)
N = 4
i_mix = []
i_3_c = []
i_5_b = []
i_5_c = []
for i in ir:
    # Mix signals
    i_mix.append(i*local_osc[lo_index])
    # Increment LO phase
    lo_index += 1
    if lo_index == N: lo_index=0 
    
i_mix = np.array(i_mix)


# Filter constants
fs = 1000.0
Ts = 1./fs
wp = (2/fs)*5
ws = (2/fs)*45
rp = 0.1
rs = 96
(order,wn)= signal.cheb2ord(wp,ws,rp,rs)
print order
(B,A)= signal.cheby2(order,rs,wn)
zf = signal.lfiltic(B,A,np.zeros(len(A)-1), np.zeros(len(B)-1))
i_5_c, zf = signal.lfilter(B,A,i_mix,zi=zf)

(B,A)= signal.butter(3,wp)
zf = signal.lfiltic(B,A,np.zeros(len(A)-1), np.zeros(len(B)-1))
i_2_b, zf = signal.lfilter(B,A,i_mix,zi=zf) 

ns = 4000
ne = 6000
trange = np.arange(len(ir))*Ts
i_2_b = i_2_b[ns:ne]
i_5_c = i_5_c[ns:ne]
trange = trange[ns:ne]

# Plot the data
plt.figure(figsize=(12,10))
ax1 = plt.subplot(1,1,1)
plt.plot(trange,2.0*i_5_c, 'k--', linewidth=2.0)
plt.plot(trange,2.0*i_2_b, 'b-', linewidth=2.0)
plt.ylabel('Filter Output',fontsize=14)
plt.xlabel('Time(s)',fontsize=14)
plt.legend(['5th order Cheybyshev (type II)','3rd order Butterworth'],loc=0)
plt.grid()
plt.subplots_adjust(left=0.07,right=0.98,top=0.97,bottom=0.07,hspace=0.2)
plt.savefig('filter_order_justification.pdf',format='pdf')
plt.show()