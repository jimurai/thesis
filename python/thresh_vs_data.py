#! /usr/bin/python
import sys
sys.path.append('embedded_dsp/Release')
sys.path.append('embedded_dsp/test')
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import h5py
import dsp

def homodyne_detector(lwdf,data):
    # Initialise homodyne detection tools
    _lo = np.array([1,0,-1,0],dtype=np.int32)    
    # Implement product detection
    _lo_i = 0
    out = np.empty((len(data),),dtype=np.int32)
    for i,d in enumerate(data):
        # Mix signals
        _mix = d*_lo[_lo_i]
        # LWDF signal
        _fout = lwdf.push(_mix)
        out[i] = (_fout[0]+_fout[1]+1)>>1
        # Increment LO phase
        _lo_i += 1
        if _lo_i == 4: _lo_i=0
    return out

def delineator(delin,data):
    # Extract the peak data
    t_p = 0
    iz = np.ones((2,),dtype=np.int32)*i_out[0]
    v_th = np.zeros((2,3),dtype=np.int32)
    t_int = np.zeros((4,),dtype=np.int32)
    # Peak detector output lists
    k_out = []
    p_out = []
    f_out = []
    df_out = []
    for d in data:
        t_p+=1
        if t_p<2: continue
        # Save history
        _dif = int(d-iz[1])
        iz[1] = iz[0]
        iz[0] = d
        # Detect peak
        _res = i_delin.write(iz[1],_dif)
        # Nom tracking?
        if _res&0x02:
            v_th[0,2] = _dif
            if _res&0x01:
                v_th[0,0] = t_int[3]
                v_th[0,1] = iz[1]
                t_int[1] = 0
            else:
                v_th[0,0] = t_int[2]
                v_th[0,1] = iz[1]
                t_int[0] = 0
        if _res&0x04:
            if _res&0x01:
                p_out.append(3)
            else:
                p_out.append(1)
            k_out.append(v_th[0,0])
            f_out.append(v_th[0,1])
            df_out.append(v_th[0,2])
        # Deriv tracking
        if _res&0x20:
            v_th[1,2] = _dif
            if _res&0x10:
                v_th[1,0] = t_int[0]
                v_th[1,1] = iz[1]
                t_int[3] = 0
            else:
                v_th[1,0] = t_int[1]
                v_th[1,1] = iz[1]
                t_int[2] = 0
        if _res&0x40:
            if _res&0x10:
                p_out.append(2)
            else:
                p_out.append(0)
            k_out.append(v_th[1,0])
            f_out.append(v_th[1,1])
            df_out.append(v_th[1,2])
        # Run any required integrators
        t_int += 1
    # Integrate time
    _k = 0
    for i,x in enumerate(k_out):
        _k += x
        k_out[i] = _k
    out = { 'i':np.array(k_out,np.int32),\
            'f':np.array(f_out,np.int32),\
            'df':np.array(df_out,np.int32),\
            'phase':np.array(p_out,np.int32)}
    return out
    
if __name__ == "__main__":
    # Homodyne tools
    fs = 1000.0
    Ts = 1.0/fs
    wp = (2/fs)*5
    ws = (2/fs)*45
    rp = 0.1
    rs = 96 
    (order,wn)= signal.cheb2ord(wp,ws,rp,rs,analog=1)
    i_filter = dsp.Filter(order,wn,rs=rs,ftype='cheby2')
    
    # Bits per extrema (samples,f[i],f'[i],phase)
    BPE = 16+32+32+2+16+16
    
    # Scaling data
    ADC_scale = 1e9*3.0/(2.0**26)
    TIA_scale = 4.38e6
    
    # Experiment lists
    data_list = [   'BSN2012/spectrum_nice',\
                    'BSN2012/spectrum_mod',\
                    'BSN2012/fast_compensation']
    threshold_list = [0.0001,0.001,0.01,0.1,0.25,0.5,1.0,1.5,2.0,2.5,3.0,5.0]
    di = 0                
    
    print("Thr(nA)\tDur(s)\tExtrema\tBits\tRate(bps)")
    for i,thr in enumerate(threshold_list):
        # Transfer from HDF5
        hfile = h5py.File('rpom_data.hdf5')
        data_set = hfile[data_list[di]]
        ir = data_set[:,0]
        hfile.close()
        
        # Kill the mean
        ir = ir - (1<<9)
        
        # Use product detection
        i_out = homodyne_detector(i_filter,ir)
        
        # Define the delineators
        _threshold = int(TIA_scale*thr/(2*ADC_scale)+0.5)
        i_delin = dsp.QuadDelineator(_threshold,100)
#        i_delin.delineator.contents.delta[:] = [int(24447),int(100)]
        # Extract the peak data
        peaks = delineator(i_delin,i_out)
        i_out = 2*ADC_scale*np.array(i_out)/TIA_scale
        di_out = (i_out[2:]-i_out[:-2])/2
        
        
        # Mask the 1st 1 seconds of data
        tr = np.nonzero((peaks['i']>1.0/Ts))
    
        # Print results of the study
        _dur = (len(i_out)-peaks['i'][tr[0][0]])*Ts
        # print("\t\t{}\t&{}\t&{}\t&{}\t&{:.1f}".format(  thr,\
                                            # _dur,\
                                            # len(tr[0]),\
                                            # BPE*len(tr[0]),\
                                            # BPE*len(tr[0])/_dur))
        print("{}\t&{}\t&{}\t&{:.1f}".format(  thr,\
                                            len(tr[0]),\
                                            BPE*len(tr[0]),\
                                            BPE*len(tr[0])/_dur))

    # Plot the data
    plt.figure(figsize=(12,8))
    ax=plt.subplot(1,1,1)
    plt.plot(np.arange(len(i_out))*Ts,i_out, 'k-', linewidth=2.0)
    plt.plot(peaks['i']*Ts,2*ADC_scale*peaks['f']/TIA_scale, 'r-',linewidth=1.0)
#    plt.plot(peaks2['i'],peaks2['f'], 'g-', linewidth=1.0)
    plt.ylabel('A Photocurrent (nA)',fontsize=16)
    plt.xlabel('Time (s)',fontsize=16)
    plt.grid(True)
#    ax=plt.subplot(2,1,2,sharex=ax)
#    plt.plot(di_out, 'k-', linewidth=2.0)
#    plt.grid(True)
       
    plt.show()