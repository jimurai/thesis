#! /usr/bin/python
import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
sys.path.append('./dsp')
import bb_dsp

# Transfer from HDF5
import h5py
hfile = h5py.File('rpom_data.hdf5')
data_set = hfile['BSN2012/spectrum_mod']
ir = data_set[:,0]
hfile.close()

# Filter constants
fs = 1000.0
wp = (2/fs)*5
ws = (2/fs)*45
rp = 0.1
rs = 96

# DSP constants
PLOTDEPTH = len(ir)
Ts = 1.0/1e3
N = 4
trange = np.arange(PLOTDEPTH)*Ts
# Frequency domain
Mn =  np.int(np.floor(PLOTDEPTH/N)*N)
frange = np.linspace(0,2,Mn)/(2.0*Ts)
window = np.hanning(Mn)

# Initialise homodyne detection tools
local_osc = np.array([1,0,-1,0],dtype=np.int32)
(order,wn)= signal.cheb2ord(wp,ws,rp,rs,analog=1)
i_filter_pd = bb_dsp.Filter(order,wn,rs=rs,ftype='cheby2')

# Use product detection
lo_index = 0
i_mix = []
i_pd = []
for i in ir:
	# Mix signals
	i_mix.append(i*local_osc[lo_index])
     # LWDF signal
	_fout = i_filter_pd.push(i_mix[-1])
	_value = (_fout[0]+_fout[1]+1)>>1
	i_pd.append(_value)
	# Increment LO phase
	lo_index += 1
	if lo_index == N: lo_index=0
i_pd = np.array(i_pd)

# Reset filter constants
fs = 250.0
wp = (2/fs)*5
ws = (2/fs)*45
(order,wn)= signal.cheb2ord(wp,ws,rp,rs,analog=1)
i_filter_cds = bb_dsp.Filter(order,wn,rs=rs,ftype='cheby2')

# Extract baseband
i_cds = []
_top = 0
_bottom = 0
for j,i in enumerate(ir):
    if j%4 == 0:
        _top = i
        _fout = i_filter_cds.push(_top-_bottom)
        _value = (_fout[0]+_fout[1]+1)>>1
        i_cds.append((_value))
    elif j%4 == 2:
        _bottom = i
i_cds = np.array(i_cds)
error = 100.0*(0.5*i_cds - 2.0*i_pd[3::4])/i_cds
# Plot the data
plt.figure(figsize=(12,8))
ax1 = plt.subplot(2,1,1)
plt.plot(trange[::4],0.5*i_cds/2.0**16, 'r-', linewidth=2.0)
plt.plot(trange,2.0*i_pd/2.0**16, 'k-', linewidth=2.0)
plt.ylabel('Filter Outputs',fontsize=14)
plt.grid()
ax2=plt.subplot(2,1,2, sharex=ax1)
plt.plot(trange[::4],error, 'r-', linewidth=2.0)
plt.ylabel('Difference (%)',fontsize=14)
plt.xlabel('Time (s)',fontsize=14)
plt.grid()
#plt.suptitle('LWDF vs. IIR Filter Response',fontsize=20)
plt.subplots_adjust(left=0.07,right=0.98,top=0.97,bottom=0.07,hspace=0.1)
plt.show()