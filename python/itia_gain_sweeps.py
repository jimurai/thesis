#! /usr/bin/python
import numpy as np
import matplotlib.pyplot as plt


# Simulation range
fs = 0.1
fe = 10e6
N = 10000
# f = np.linspace(fs,fe,N)
# Generate Laplace range
f = np.logspace(np.log10(fs),np.log10(fe),N)
w = 2*np.pi*f
s = 1j*w

# Cf sweep
C_sh_nom = 10e-9
GBW_nom = 500e3
C_f_nom = 100e-12
A_0 = 1000
R_sh = 1e9

C_f = [1e-12, 10e-12, 100e-12, 1e-9, 10e-9]
C_f_lbl = ['1pF', '10pF','100pF', '1nF', '10nF']

# Generate Laplace models
H = []
for c in C_f:
	# 1st order op-amp integrator model
	numer = 1
	b2 = (c+C_sh_nom)/(2*np.pi*GBW_nom)
	b1 = c + (c+C_sh_nom)/A_0 + 1/(2*np.pi*GBW_nom*R_sh)
	# b1 = c + (c+C_sh_nom)/A_0
	b0 = 1.0/(A_0*R_sh)
	denom =	s*s*b2 + s*b1 + b0
	# denom =	s*s*b2 + s*b1
	H.append(numer/denom)

gain = plt.figure(0,figsize=(8,6))
plt.subplot(1,1,1)
for h in H:
	plt.semilogx(f,20*np.log10(np.abs(h)),linewidth=2)
# plt.semilogx(f,20*np.log10(np.abs(H_1)),'r')
plt.grid(True)
plt.xlabel('Frequency (Hz)')
plt.subplots_adjust(left=0.10,right=0.97,top=0.95,bottom=0.10)
plt.ylabel('Transimpedance Gain(dB)')
plt.title('{}={}nF, $GBW$={}kHz'.format('$C_{sh}$',
										int(C_sh_nom/1e-9),
										int(GBW_nom/1e3)))
plt.legend(C_f_lbl)
plt.savefig('itia_gain_cf.svg',format='svg')

# Csh sweep
C_sh = [10e-12, 100e-12, 1e-9, 10e-9, 100e-9]
C_sh_lbl = ['10pF','100pF', '1nF', '10nF', '100nF']

# Generate Laplace models
H = []
for c in C_sh:
	# 1st order op-amp integrator model
	numer = 1
	b2 = (C_f_nom+c)/(2*np.pi*GBW_nom)
	b1 = C_f_nom + (C_f_nom+c)/A_0 + 1/(2*np.pi*GBW_nom*R_sh)
	# b1 = c + (C_f_nom+C_sh)/A_0
	b0 = 1.0/(A_0*R_sh)
	denom =	s*s*b2 + s*b1 + b0
	# denom =	s*s*b2 + s*b1
	H.append(numer/denom)

gain = plt.figure(1,figsize=(8,6))
plt.subplot(1,1,1)
for h in H:
	plt.semilogx(f,20*np.log10(np.abs(h)),linewidth=2)
# plt.semilogx(f,20*np.log10(np.abs(H_1)),'r')
plt.grid(True)
plt.xlabel('Frequency (Hz)')
plt.subplots_adjust(left=0.10,right=0.97,top=0.95,bottom=0.10)
plt.ylabel('Transimpedance Gain(dB)')
plt.title('$C_{f}$=100pF, $GBW$=1MHz')
plt.title('{}={}pF, $GBW$={}kHz'.format('$C_{f}$',
										int(C_f_nom/1e-12),
										int(GBW_nom/1e3)))
plt.legend(C_sh_lbl)
plt.savefig('itia_gain_csh.svg',format='svg')

# GBW sweep
GBW = [10e3, 100e3, 1e6, 10e6]
GBW_lbl = ['10kHz', '100kHz','1MHz', '10MHz']
H = []
for g in GBW:
	# 1st order op-amp integrator model
	numer = 1
	b2 = (C_f_nom+C_sh_nom)/(2*np.pi*g)
	b1 = C_f_nom + (C_f_nom+C_sh_nom)/A_0 + 1/(2*np.pi*g*R_sh)
	# b1 = C_f_nom + (C_f_nom+C_sh_nom)/A_0
	b0 = 1.0/(A_0*R_sh)
	denom =	s*s*b2 + s*b1 + b0
	# denom =	s*s*b2 + s*b1
	H.append(numer/denom)

gain = plt.figure(2,figsize=(8,6))
plt.subplot(1,1,1)
for h in H:
	plt.semilogx(f,20*np.log10(np.abs(h)),linewidth=2)
# plt.semilogx(f,20*np.log10(np.abs(H_1)),'r')
plt.grid(True)
plt.xlabel('Frequency (Hz)')
plt.subplots_adjust(left=0.10,right=0.97,top=0.95,bottom=0.10)
plt.ylabel('Transimpedance Gain(dB)')
plt.title('{}={}pF, {}={}nF'.format('$C_{f}$',
									int(C_f_nom/1e-12),
									'$C_{sh}$',
									int(C_sh_nom/1e-9)))
plt.legend(GBW_lbl)
plt.savefig('itia_gain_gbw.svg',format='svg')

plt.show()