#! /usr/bin/python
import sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import host_subplot
from scipy import fftpack, signal
sys.path.append('./dsp')
import bb_dsp

# Transfer from HDF5
import h5py
hfile = h5py.File('rpom_data.hdf5')
data_set = hfile['BSN2012/spectrum_mod']
ir = data_set[:,0]
hfile.close()

# Filter constants
fs = 1000.0
wp = (2/fs)*5
ws = (2/fs)*45
rp = 0.1
rs = 96

# DSP constants
PLOTDEPTH = len(ir)
Ts = 1.0/1e3
N = 4
trange = np.arange(PLOTDEPTH)*Ts
# Frequency domain
Mn =  np.int(np.floor(PLOTDEPTH/N)*N)
frange = np.linspace(0,2,Mn)/(2.0*Ts)
window = np.hanning(Mn)

# Initialise homodyne detection tools
local_osc = np.array([1,0,-1,0],dtype=np.int32)
(order,wn)= signal.cheb2ord(wp,ws,rp,rs,analog=1)
i_filter_bb = bb_dsp.Filter(order,wn,rs=rs,ftype='cheby2')
i_filter_pd = bb_dsp.Filter(order,wn,rs=rs,ftype='cheby2')
i_filter_ed = bb_dsp.Filter(order,wn,rs=rs,ftype='cheby2')

# Extract baseband
i_bb = []
for i in ir:
	_fout = i_filter_bb.push(i)
	_value = (_fout[0]+_fout[1]+1)>>1
	i_bb.append((_value))
i_bb = np.array(i_bb)

# Use product detection
lo_index = 0
i_mix = []
i_pd = []
for i in ir:
	# Mix signals
	i_mix.append(i*local_osc[lo_index])
     # LWDF signal
	_fout = i_filter_pd.push(i_mix[-1])
	_value = (_fout[0]+_fout[1]+1)>>1
	i_pd.append(_value)
	# Increment LO phase
	lo_index += 1
	if lo_index == N: lo_index=0
i_pd = np.array(i_pd)

# Use envelope detection
P = 4
sP = int(np.log2(P))
i_ed = np.zeros((len(ir),),dtype=np.int64)
_i_hist = np.zeros((P,),dtype=np.int64)
i_ed_filt = []
for j,i in enumerate(ir):
    # Shift history back in time
    _i_hist[1:] = _i_hist[:-1]
    _i_hist[0] = i
    # STDDEV = mean of squares - square of means
    i_ed[j] = np.sqrt((np.sum(_i_hist*_i_hist)>>sP)\
              - ((np.sum(_i_hist)>>sP)**2))
    # LWDF signal
    _fout = i_filter_ed.push(i_ed[j])
    _value = (_fout[0]+_fout[1]+1)>>1
    i_ed_filt.append(_value)
i_ed_filt = np.array(i_ed_filt)

# Plot the data
plt.figure(figsize=(12,8))
ax1 = plt.subplot(3,1,1)
plt.plot(trange,ir, 'k-', linewidth=2.0)
plt.ylabel('Input Signal',fontsize=14)
plt.grid()
#host = host_subplot(3,1,2)
#par = host.twinx()
ax2=plt.subplot(3,1,2, sharex=ax1)
plt.plot(trange,i_bb/2.0**16, 'k-', linewidth=2.0)
plt.ylabel('Baseband Output',fontsize=14)
plt.grid()
ax3=plt.subplot(3,1,3, sharex=ax1)
plt.plot(trange,2*i_pd/2.0**16, 'b-', linewidth=2.0)
plt.plot(trange,np.sqrt(2)*i_ed_filt/2.0**16, 'g-', linewidth=2.0)
plt.legend(['Envelope Detection','Product Detection'],loc='lower right')
plt.ylabel('Homodyne Band Output',fontsize=14)
plt.xlabel('Time (s)')
#plt.ylabel('Envelope Detection Output',fontsize=14)
plt.grid()
#plt.suptitle('LWDF vs. IIR Filter Response',fontsize=20)
plt.subplots_adjust(left=0.07,right=0.97,top=0.97,bottom=0.05,hspace=0.1)
#
#plt.figure(figsize=(10,6))
#ax4=plt.subplot(2,1,1)
#plt.plot(trange,i_iir, 'r-', linewidth=2.0)
#plt.plot(trange,i_lwdf_16b, 'k-', linewidth=2.0)
#plt.ylabel('Filtered Signal',fontsize=14)
#plt.grid()
#ax5=plt.subplot(2,1,2, sharex=ax4)
#plt.plot(trange,i_lwdf_16b-i_iir, 'r-', linewidth=2.0)
#plt.ylabel('Error',fontsize=16)
#plt.xlabel('Time (s)',fontsize=14)
#plt.grid()
#plt.subplots_adjust(left=0.09,right=0.97,top=0.97,bottom=0.08,hspace=0.14)

plt.show()