#! /usr/bin/python
import sys
import numpy as np
import matplotlib.pyplot as plt

# Transfer from HDF5
import h5py
hfile = h5py.File('v3_data.hdf5')
group_list = list(hfile)
exps = hfile['v3data']
exp_list = list(exps)
data_set = exps[exp_list[-1]]
ir = data_set[:,0]
rd = data_set[:,1]
hfile.close()


# DSP constants
PLOTDEPTH = len(ir)
Ts = 1.0/1e3
trange = np.arange(PLOTDEPTH)*Ts

# Scale data
ADC_scale = 2*3.0/((2.0**12)*64)
TIA_scale = 3e6
i_out = 1e9*ADC_scale*np.array(ir)/TIA_scale
r_out = 1e9*ADC_scale*np.array(rd)/TIA_scale

# Use produ
# Plot the data
plt.figure(figsize=(12,8))
ax1 = plt.subplot(2,1,1)
plt.plot(trange,i_out, 'k-', linewidth=2.0)
#plt.xlabel('Time (s)',fontsize=14)
plt.ylabel('IR Photocurrent (nA)',fontsize=14)
plt.grid()
ax2 = plt.subplot(2,1,2,sharex=ax1)
plt.plot(trange,r_out, 'r-', linewidth=2.0)
plt.xlabel('Time (s)',fontsize=14)
plt.ylabel('Red Photocurrent (nA)',fontsize=14)
plt.grid()
#plt.suptitle('LWDF vs. IIR Filter Response',fontsize=20)
plt.subplots_adjust(left=0.07,right=0.98,top=0.97,bottom=0.07,hspace=0.1)
plt.savefig('../figures/v3_data.pdf',format='pdf')
plt.show()