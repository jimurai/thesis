#! /usr/bin/python
import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy import fftpack
sys.path.append('./dsp')
import bb_dsp

# Transfer from HDF5
import h5py
hfile = h5py.File('rpom_data.hdf5')
data_set = hfile['BSN2012/spectrum_nice']
ir = data_set[:,0]
hfile.close()

# DSP constants
PLOTDEPTH = len(ir)
Ts = 1.0/1e3
N = 4
# Frequency domain
Mn =  np.int(np.floor(PLOTDEPTH/N)*N)
frange = np.linspace(0,2,Mn)/(2.0*Ts)
window = np.hanning(Mn)

# Initialise homodyne detection tools
local_osc = np.array([1,0,-1,0],dtype=np.int32)
i_filter = bb_dsp.Filter(5,20.0*2*Ts,rs=90,ftype='cheby2')

# Mix input with local oscillator
lo_index = 0
i_mix = []
for i in ir:
	# Mix signals
	i_mix.append(i*local_osc[lo_index])
	# Increment LO phase
	lo_index += 1
	if lo_index == N: lo_index=0
	
	
# Frequency analysis
i_dft = 20.0*np.log10(np.abs(fftpack.fft(window*i_mix[-Mn:])))

# Plot the data
plt.figure(figsize=(10,8))
plt.plot(frange[:920],i_dft[:920]-i_dft[0], 'k')
plt.title('Unfiltered Homodyne Signal', fontsize=20)
plt.xlabel('Frequency (Hz)', fontsize=16)
plt.ylabel('Amplitude (dB)', fontsize=16)
plt.grid()
plt.subplots_adjust(left=0.1,right=0.97,top=0.94,bottom=0.07,hspace=0.14)
# plt.savefig('../src/filter_bw_justification.svg',format='svg')
plt.show()