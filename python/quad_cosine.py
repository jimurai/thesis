import numpy as np
import matplotlib.pyplot as plt

# Constants
N = 10001

# Signals
t = np.linspace(0,2.0,N)
x = np.cos(np.pi*t)
r0 = np.nonzero(t<0.5)
r1 = np.nonzero((t>=0.5)*(t<1))
r2 = np.nonzero((t>=1)*(t<1.5))
r3 = np.nonzero((t>=1.5)*(t<=2))


# Plot signals
fig = plt.figure(figsize=(12,8))
ax = fig.add_subplot(1,1,1)
plt.plot(t, x, 'k-', linewidth=2)
plt.ylabel('Amplitude',fontsize=16)
plt.xlabel('Phase (radians)',fontsize=16)
plt.yticks(fontsize=15)
plt.xticks((0,0.5,1,1.5,2),('0','$\pi/2$','$\pi/2$','$3\pi/2$','$2\pi$'),fontsize=16)

ax.fill_between(t[r0], 0, x[r0], facecolor='b', interpolate=True,alpha=0.3)
ax.text(0.25,0.25,'$\phi_0$',fontsize=64,ha='right')
ax.fill_between(t[r1], 0, x[r1], facecolor='g', interpolate=True,alpha=0.3)
ax.text(0.75,-0.4,'$\phi_1$',fontsize=64,ha='left')
ax.fill_between(t[r2], 0, x[r2], facecolor='r', interpolate=True,alpha=0.3)
ax.text(1.25,-0.4,'$\phi_2$',fontsize=64,ha='right')
ax.fill_between(t[r3], 0, x[r3], facecolor='k', interpolate=True,alpha=0.3)
ax.text(1.75,0.25,'$\phi_3$',fontsize=64,ha='left')
plt.grid(True)

plt.subplots_adjust(left=0.08,right=0.97,top=0.97,bottom=0.09)
plt.savefig('../figures/quad_cosine.pdf',format='pdf')
plt.show()