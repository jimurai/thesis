#! /usr/bin/python
import numpy as np
import matplotlib.pyplot as plt

# Simulation range
fs = 0.001
fe = 1e5
N = 10000
# f = np.linspace(fs,fe,N)
f = np.logspace(np.log10(fs),np.log10(fe),N)
w = 2*np.pi*f
s = 1j*w

# Generate Laplace models

# BSN2012
Ts = 1e-3
wf = 0.25*(2*np.pi)
a0 = (1<<10)/3.0
a1 = (1<<7)
a2 = 1.25/(1<<12)
R_f = 330e3
C_f = 100e-12
aT = a0*a1*a2

K = ((Ts/C_f)*(aT/R_f)+1)*wf
print "Pole at", K/(2*np.pi), "Hz"
numer = 1
denom = s*C_f + s*Ts*aT/R_f/(1 + s/wf)
#numer = 1
#denom = s*C_f
H_0 = numer/denom
G_0 = 20*np.log10(np.abs(H_0[0]))
P_0 = np.arctan2(H_0.real,H_0.imag)

aT = a0*a2*a1
numer = 1
denom = s*C_f + s*Ts*aT/R_f/s
H_1 = numer/denom
G_1 = 20*np.log10(np.abs(H_1[0]))
P_1 = np.arctan2(H_1.real,H_1.imag)
print "DC gain =",G_1,"dB"

b = -(1<<5)
c = (1<<7)
numer = 1
# numer = 1 + s/wf
denom = s*C_f + ((s*Ts*c + b)/s)*a0*a2/R_f
H_2 = numer/denom
G_2 = 20*np.log10(np.abs(H_2[0]))
P_2 = np.arctan2(H_2.real,H_2.imag)
print "DC gain =",G_2,"dB"


plt.figure(figsize=(8,8))
plt.subplot(2,1,1)
plt.semilogx(f,20*np.log10(np.abs(H_0)),'r')
#plt.semilogx(f,20*np.log10(np.abs(H_1)),'r')
plt.semilogx(f,20*np.log10(np.abs(H_2)),'k',linewidth=2)
plt.ylabel('Transimpedance Gain (dB)')
plt.legend(['Original Design','Optimised Design'])
plt.grid(True)
plt.subplot(2,1,2)
plt.semilogx(f,P_0*180/np.pi,'r')
#plt.semilogx(f,P_1*180/np.pi,'r')
plt.semilogx(f,P_2*180/np.pi,'k',linewidth=2)
plt.ylabel('Phase (degrees)')
plt.xlabel('Frequency (Hz)')
plt.grid(True)
plt.subplots_adjust(left=0.09,right=0.97,top=0.94,bottom=0.07)
plt.suptitle('$A_{FB}=-2^5$, $B_{FB}=2^7$',fontsize='large')
plt.show()