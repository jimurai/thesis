#! /usr/bin/python
import sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import host_subplot
from scipy import fftpack, signal
sys.path.append('./dsp')
import bb_dsp

# Constants
fs = 1000.0
wp = (2/fs)*5
ws = (2/fs)*45
rp = 0.1
rs = 96
N = 10000

# Axes
t = np.linspace(0,N/fs,N,endpoint=False)
f = np.linspace(0,2,N)*fs/2.0
Nf = np.nonzero(f<fs/2)[0][-1]

# Create impulse
peak = (1<<14)
impulse_in = np.zeros((N,),np.int16)
impulse_in[0] = peak

# IIR Filter
(order,wn)= signal.cheb2ord(wp,ws,rp,rs,analog=1)
print order, wn*fs/2
(B,A)= signal.cheby2(order,rs,wn,btype='lowpass',output='ba')
zf = signal.lfiltic(B,A,np.zeros(len(A)-1), np.zeros(len(B)-1))
print B, A
iir_out, zf = signal.lfilter(B,A,impulse_in,zi=zf)

# LWDF
lwdf = bb_dsp.Filter(order,wn,rs=rs,ftype='cheby2')
lwdf_out = []
for x in impulse_in:
    _fout = lwdf.push(x)
    _value = (_fout[0]+_fout[1]+1)>>1
    lwdf_out.append(_value)
lwdf_out = np.array(lwdf_out)

# Frequency domain
F_lwdf = fftpack.fft(lwdf_out)/(peak*(1<<16))
F_iir = fftpack.fft(iir_out)/peak
dB_lwdf = 20*np.log10(np.abs(F_lwdf))
dB_iir = 20*np.log10(np.abs(F_iir))
arg_lwdf = np.arctan2(F_lwdf.real,F_lwdf.imag)
arg_iir = np.arctan2(F_iir.real,F_iir.imag)

# Group delay
tau_lwdf = np.zeros_like(f)
tau_iir = np.zeros_like(f)
w = 2*np.pi*f[:Nf]
for i in range(len(w)):
	tmp = 0
	if (i!=0) and (i!=len(w)-1):
		tmp = (arg_lwdf[i+1]-arg_lwdf[i-1])
		if tmp<-np.pi:
			tmp+=2*np.pi
		tmp = tmp/(w[i+1]-w[i-1])
	tau_lwdf[i] = tmp
	tmp = 0
	if (i!=0) and (i!=len(w)-1):
		tmp = (arg_iir[i+1]-arg_iir[i-1])
		if tmp<-np.pi:
			tmp+=2*np.pi
		tmp = tmp/(w[i+1]-w[i-1])
	tau_iir[i] = tmp
 
# Plotting
fig = plt.figure(figsize=(12,8))
plt.subplot(2,1,1)
#plt.plot(t,lwdf_out-iir_out)
#plt.semilogx(f[:Nf],20*np.log10(F_err))
plt.semilogx(f[:Nf],dB_lwdf[:Nf],'k-',linewidth=2)
plt.semilogx(f[:Nf],dB_iir[:Nf],'r--',linewidth=1.5)
plt.ylabel('Gain Response (dB)')
plt.xlabel('Frequency (Hz)')
plt.legend(['LWDF','IIR'])
plt.grid(True)
plt.subplot(2,2,3)
plt.semilogx(f[:Nf],arg_lwdf[:Nf]*180/np.pi,'k-',linewidth=2)
plt.semilogx(f[:Nf],arg_iir[:Nf]*180/np.pi,'r--',linewidth=1.5)
plt.ylabel('Phase Response (degrees)')
plt.xlabel('Frequency (Hz)')
plt.grid(True)
plt.subplot(2,2,4)
print len(tau_lwdf)
plt.semilogx(f[1:Nf],1000*tau_lwdf[1:Nf],'k-',linewidth=2)
plt.semilogx(f[1:Nf],1000*tau_iir[1:Nf],'r--',linewidth=1.5)
plt.ylabel('Group Delay (ms)')
plt.xlabel('Frequency (Hz)')
plt.grid(True)

plt.suptitle('LWDF vs. IIR Filter Response',fontsize=20)
plt.subplots_adjust(left=0.07,right=0.94,top=0.93,bottom=0.08,hspace=0.3,wspace=0.3)
#plt.savefig('../figures/homodyne_impulse_response.pdf',format='pdf')
plt.show()